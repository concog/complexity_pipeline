function [r3, p3, q1, q2, group1, group2] = reorder_correlation_matrix(r, p, queries1, queries2, varargin)

% The clustering algorithms used in this function are not particularly
% robust. A better function is needed.

params = struct('threshold', 0.8, 'pca', 0);
params = parse_varargin(params, varargin{:});

if nargin > 3 && ~isempty(queries1) && ~isempty(queries2)
    queries1 = parse_queries(queries1);
    queries2 = parse_queries(queries2);
end


%if nargin < 5
    %k_values = unique([2:min(size(r,1)/3, 10), round(prctile(1:size(r,1), [1:10, 12:2:20, 25:5:30]))]);
    %eva = evalclusters(r, 'kmeans', 'CalinskiHarabasz', 'KList', k_values);
    %k = eva.OptimalK;
%end
%idx = kmeans(r, k, 'emptyaction','drop','replicates',100,'maxiter',500);

if params.pca
    coeff = pca(r');
    d = coeff(:,1:params.pca);
else
    d = r;
end
[idx,~,~,k] = kmeans_opt(d, ceil(sqrt(size(d,1))), params.threshold,100);

r2 = [];
p2 = [];
group2 = [];
q2={};

for i = 1:k
    if nargin > 3 && ~isempty(queries2)
        q2(end+1:end+sum(idx==i)) = queries2(idx==i);
    end
    if nargin > 1 && ~isempty(p)
        p2 = [p2; p(idx==i,:)];
    end
    r2 = [r2; r(idx==i,:)];
    group2(end+1:end+sum(idx==i)) = i;
end

%if nargin < 5
%    k_values = unique([1:10, round(prctile(1:size(r,1), [1:10, 12:2:20, 30:10:50]))]);
%    eva = evalclusters(r2', 'kmeans', 'CalinskiHarabasz', 'KList', k_values);
%    k = eva.OptimalK;
%end
%idx = kmeans(r2', k, 'emptyaction', 'drop', 'replicates', 5);

if params.pca
    coeff = pca(r2);
    d = coeff(:, 1:params.pca);
else
    d = r2';
end
[idx,~,~,k] = kmeans_opt(d, ceil(sqrt(size(d,1))), params.threshold,100);
%[idx,~,~,k] = kmeans_opt(r',ceil(sqrt(size(r,2))),threshold,10);

r3 = [];
p3 = [];
group1 = [];
q1 = {};

for i = 1:k
    if nargin > 2 && ~isempty(queries1)
        q1(end+1:end+sum(idx==i)) = queries1(idx==i);
    end
    if nargin > 1 && ~isempty(p)
        p3 = [p3, p2(:, idx==i)];
    end
    r3 = [r3, r2(:, idx==i)];
    group1(end+1:end+sum(idx==i)) = i;
end

end