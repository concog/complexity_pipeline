function fig2 = difference_between_tasks(cdataset1, cdataset2, queries, varargin)
%DIFFERENCE_BETWEEN_TASKS Difference between tasks for same group of
%subjects
%   Make sure you have defined timepoints (if necessary)

% Find common subjects
mapping = common_subjects(cdataset1, cdataset2);

% Get the data
data1 = cdataset1.get_data(queries, varargin{:}, 'format', 'medians', 'subjects', mapping(:,1));
data2 = cdataset2.get_data(queries, varargin{:}, 'format', 'medians', 'subjects', mapping(:,2));

% Do a t-test
%[~, p] = ttest(data1, data2);
dfrnc = data1 - data2;
p = signrank_array(dfrnc);

% Do Benjamini-Hochberg FDR correction
[h, p_bh] = fdr_bh(p);

% Calculate the difference in medians; actually, divide also by std so
% they're roughly in the same scale
dfrnc = median(dfrnc);
%dfrnc = mean(dfrnc)./std(dfrnc);

% Then do some plotting
%fig1 = get_figure(mfilename, 1); hold on;
%fig1 = figure(); hold on;

% Plot significants
%sig_h = scatter(find(h), dfrnc(find(h)), 100, 'o', 'MarkerFaceColor', 'k', 'markeredgecolor', 'k');

% Plot non-significants
%nonsig_h = scatter(find(~h), dfrnc(find(~h)), 100, 'o', 'markeredgecolor', 'k', 'markerfacealpha', 0);

% Plot zero line
%plot([0, numel(h)+1], [0, 0], 'k--');

% Set legends, titles, etc.
%legend([sig_h, nonsig_h], ["Significant difference", "Non-significant difference"], 'location', 'best');
%title({'Difference between tasks', 'scaled by standard deviations'});

%hdt = datacursormode;
%set(hdt,'UpdateFcn',{@labeltips1d, parse_queries(queries), 'Difference'})

% Manhattan style plot
%fig2 = get_figure([mfilename '_manhattan'], 1); hold on;
fig2 = figure(); hold on;

% Find positive differences
pos = dfrnc >= 0;
neg = dfrnc < 0;

% Plot negatives
scatter(find(neg), -log10(p(neg)), 100, '^', 'markerfacecolor', 'g', 'markeredgecolor', 'g');

% Plot positives
scatter(find(pos), -log10(p(pos)), 100, 'v', 'markerfacecolor', 'r', 'markeredgecolor', 'r');

% Plot Bonferroni threshold
plot([0, numel(h)+1], [-log10(0.05/numel(h)), -log10(0.05/numel(h))], 'k--', 'linewidth', 2);

% Plot Benjamini-Hochberg threshold
plot([0, numel(h)+1], [-log10(p_bh), -log10(p_bh)], 'm--', 'linewidth', 2);

% Titles and such
task1 = strjoin(cdataset1.name, ' / ');
task2 = strjoin(cdataset2.name, ' / ');
legend([task2 + " > " + task1, task2 + " < " + task1, "Bonferroni", "Benjamini-Hochberg"], 'location', 'best');
title({"Difference between tasks", task1 + " and " + task2});
xlabel('Feature'); ylabel('-log10(p)');

hdt = datacursormode;
set(hdt,'UpdateFcn',{@labeltips1d, parse_queries(queries), 'p', @(x) 10^(-x)})

end

