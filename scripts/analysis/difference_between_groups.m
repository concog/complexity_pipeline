function fig = difference_between_groups(dataset1, subset1, dataset2, subset2, queries)

if iscell(subset1)
    subset1_name = string(subset1{1});
    subset1 = subset1{2};
else
    subset1_name = "group1";
end

if iscell(subset2)
    subset2_name = string(subset2{1});
    subset2 = subset2{2};
else
    subset2_name = "group2";
end

% Grab the data
data1 = dataset1.get_data(queries, 'format', 'medians', 'subjects', subset1);
data2 = dataset2.get_data(queries, 'format', 'medians', 'subjects', subset2);

% Ranksum tests
p = ranksum_array(data1, data2);

% FDR corrected
[h, p_bh] = fdr_bh(p);

% Find positives and negatives
pos = median(data1)-median(data2) >= 0;
neg = ~pos;

fig = figure(); hold on;
scatter(find(neg), -log10(p(neg)), 100, '^', 'markerfacecolor', 'g', 'markeredgecolor', 'g');
scatter(find(pos), -log10(p(pos)), 100, 'v', 'markerfacecolor', 'r', 'markeredgecolor', 'r');
plot([0, numel(h)+1], [-log10(0.05/numel(h)), -log10(0.05/numel(h))], 'r--', 'linewidth', 2);
plot([0, numel(h)+1], [-log10(p_bh), -log10(p_bh)], 'm--', 'linewidth', 2);

ylabel('-log10(p)');
xlabel('Feature');
legend(subset2_name + " > " + subset1_name, subset2_name + " < " + subset1_name, 'Bonferroni', 'Benjamini-Hochberg', 'location', 'best');
title({"Difference between groups:", subset1_name + " and " + subset2_name});

hdt = datacursormode;
set(hdt,'UpdateFcn',{@labeltips1d, parse_queries(queries), 'p', @(x) 10^(-x)});

end