function mdl = train_model(model_name, features, response, varargin)

% Features may be a nsubjects x nfeatures x ntimepoints array, thus we may
% need to modify responses if ntimepoints > 1
[~, ~, ntimepoints] = size(features);
if size(response, 3) == 1
    response = repmat(response, 1, 1, ntimepoints);
end

features = reformat(features);
response = reformat(response);

switch lower(model_name)
    case "glm"
        mdl = fitglm(features, response, 'linear', 'Distribution', 'inverse gaussian');
        
    case "linear"
        mdl = fitlm(features, response);

    case "glm_int"
        mdl = fitglm(features, response, 'interactions', 'Distribution', 'inverse gaussian');

    case "gp"
        mdl = fitrgp(features, response, 'Standardize', true, 'DistanceMethod', 'accurate', 'CacheSize', 3000);
%             'OptimizeHyperparameters',{'BasisFunction', 'Sigma'}, ...
%             'HyperparameterOptimizationOptions', struct('AcquisitionFunctionName','expected-improvement-plus'));

    case "svm"
        if isnumeric(response)
            mdl = fitrsvm(features, response, 'Standardize', true, 'KernelFunction', 'gaussian', 'KernelScale', 'auto');
%             'OptimizeHyperparameters', {'KernelScale', 'Epsilon'}, ...
%             'HyperparameterOptimizationOptions', struct('AcquisitionFUnctionName','expected-improvement-plus'));
        else
            mdl = fitcsvm(features, response, 'Standardize', true, 'KernelFunction', 'gaussian', 'KernelScale', 'auto');
        end
        
    case "libsvm"
        addpath /home/ai04/Workspace/software/libsvm-3.22/
        addpath /home/ai04/Workspace/software/libsvm-3.22/matlab
        type = sprintf('-s %d -t %d -c %f -g %f', 0, 2, 2^0, 2^-6);
        tmp = false(size(response));
        tmp(response=="patient") = true;
        mdl = svmtrain(double(tmp), features, type);
        
    case "ensemble"
        mdl = fitcensemble(features, response);

    case "naive"
        mdl = "Naive";
        
    case "pls"
        mdl = PLSRegression(features, response);
        
    case "plsda"
        mdl = PLSDA(features, response);

    otherwise
        msg = ['Model name ' model_name ' not recognized'];
        error(msg);
end
        
end