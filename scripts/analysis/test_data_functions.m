function test_data_functions(dataset, queries)

nreps = max(round(0.1*dataset.nsubjects),1);

% Make sure all necessary queries are loaded
queries = parse_queries(queries);
dataset = dataset.add_features(queries);

% Get locations of subjects' data
subj_files = dataset.get_data({{'info','files','file_processed'}});


for rep = 1:nreps

    % Choose randomly a subject
    rand_subject_idx = randsample(1:dataset.nsubjects,1);
    
    % Load subjects' data
    rand_subject = jsondecode(fileread(subj_files{rand_subject_idx}));
    
    % Choose some random queries
    rand_queries_num = randsample(1:numel(queries),1);
    rand_queries_idx = randsample(1:numel(queries), rand_queries_num);

    
    % Load data
    data = dataset.get_data(queries(rand_queries_idx));
    
    % Go through data and make sure it's equal to rand_subject
    if iscell(data)
        for feat_idx = 1:numel(data)
            otherdims = repmat({':'},1,ndims(data{feat_idx})-1);
            d1 = data{feat_idx}(rand_subject_idx, :, otherdims{:});
            d1 = squeeze(permute(d1, [2, 1, 3:ndims(d1)]));
            
            query = queries{rand_queries_idx(feat_idx)};
            d2_timepoints = getfield(rand_subject, query{:});
            if isstruct(d2_timepoints)
                d2 = cell2mat(struct2cell(d2_timepoints));
                % Need to sort timepoints in d2
                [~, sortidx] = sort_nat(fieldnames(d2_timepoints));
                d2 = d2(sortidx);
            else
                d2 = d2_timepoints;
            end
                        
            if ~isequal(d1, d2)
                if (isempty(d1) || isnan(d1)) && (isempty(d2) || isnan(d2))
                    continue
                end
                error('Something''s wrong with subject %d feature %s', rand_subject_idx, strjoin(query,' / '));
            end
        end
    else
        otherdims = repmat({':'},1,ndims(data)-3);
        for feat_idx = 1:size(data,2)
            d1 = squeeze(data(rand_subject_idx, feat_idx, :, otherdims{:}));
            
            query = queries{rand_queries_idx(feat_idx)};
            d2_timepoints = getfield(rand_subject, query{:});
            d2 = cell2mat(struct2cell(d2_timepoints));
            
            % Need to sort timepoints in d2
            [~, sortidx] = sort_nat(fieldnames(d2_timepoints));
            
            if ~isequal(d1, d2(sortidx))
                if (isempty(d1) || isnan(d1)) && (isempty(d2) || isnan(d2))
                    continue
                end
                error('Something''s wrong with subject %d feature %s', rand_subject_idx, strjoin(query,' / '));
            end
        end
    end
    fprintf('subject %d: %d features ok\n', rand_subject_idx, feat_idx);
end
end