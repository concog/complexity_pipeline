function fig = plot_as_function_of_X(X, features, Xq, queries, varargin)

params = struct('ngroups', [], 'remove_outliers', false, 'correlation_type', 'pearson');
params = parse_varargin(params, varargin{:});

if ~isempty(params.ngroups)
    % Divide subjects into 10 groups
    groups = quantileranks(X, 10);

    % Get age medians
    X_meds = calculate_medians(groups, X);
    
    markersize = 200;
    alpha = 1;
else
    X_meds = X;
    markersize = 20;
    alpha = 0.3;
    
    if params.remove_outliers
        outliers = isoutlier(X_meds, 'quartiles');
        X_meds(outliers) = nan;
    end
end


fig = figure(); hold on;

legs = cell(numel(queries), 1);
legs_h = zeros(numel(queries), 1);
for feature_idx = 1:numel(queries)
    
    if ~isempty(params.ngroups)
        % Get medians
        feat_meds = calculate_medians(groups, features(:,feature_idx));
    else
        feat_meds = features(:,feature_idx);
        
        if params.remove_outliers
            outliers = isoutlier(feat_meds, 'quartiles');
            feat_meds(outliers) = nan;
        end
    end
    
    % Plot points
    h_points = scatter(X_meds, feat_meds, markersize, '.', 'markerfacealpha', alpha);

    % If outliers have been removed fit a linear model to get Pearson's r. If
    % outliers haven't been removed, fit a robust linear regression line
    % and calculate Spearman's rho (more robust to outliers than Pearson's
    % r)
    min_X = min(X_meds);
    max_X = max(X_meds);
    if params.correlation_type == "pearson"
        % Fit a regression line
        mdl = fitlm(X_meds, feat_meds);

        % For plotting regression line
        min_predict = mdl.predict(min_X);
        max_predict = mdl.predict(max_X);
        
        % Calculate eta squared effect size
        stats = mes1way([feat_meds, X_meds], 'omega2', 'isDep', 1);

        legs_msg = sprintf('%s: Pearson r = %f, p = %f -- eta^2 = %f', strjoin(queries{feature_idx}, ' / '), ...
            sign(mdl.Coefficients.Estimate(2))*sqrt(mdl.Rsquared.Ordinary), mdl.Coefficients.pValue(2), stats.omega2);
    else
       % Fit a robust linear regression line
       nans = isnan(X_meds) | isnan(feat_meds);
       mdl = fit(X_meds(~nans), feat_meds(~nans), 'poly1', 'robust', 'bisquare');
       
       % For plotting regression line
       min_predict = mdl(min_X);
       max_predict = mdl(max_X);
       
       [r,p] = corr(X_meds, feat_meds, 'type', 'spearman', 'rows', 'pairwise');
       
       legs_msg = sprintf('%s: Spearman rho = %f, p = %f', strjoin(queries{feature_idx}, ' / '), ...
            r, p);
    end

    h_line = plot([min_X, max_X], [min_predict, max_predict], 'linewidth', 2, 'color', h_points.CData);
    legs_h(feature_idx) = h_line;
    legs{feature_idx} = legs_msg;
end


title(sprintf('Features as a function of %s', strjoin(Xq{1},' / '))); 
xlabel(strjoin(Xq{1},' / '));
legend(legs_h, legs, 'location', 'southoutside', 'interpreter', 'none');
    
end

function medians = calculate_medians(groups, data)

% There may be zero-elements in 'groups', these are ignored
uniq_groups = unique(groups);
uniq_groups(uniq_groups==0) = [];

medians = nan(numel(uniq_groups), 1);
for group_idx = 1:numel(uniq_groups)
    medians(group_idx) = median(data(groups==uniq_groups(group_idx)));
end

end