function queries = srivas_queries(conf_types, bands, metrics)

if nargin < 1 || isempty(conf_types)
    conf_types = ["combined", "averaged", "odd", "even"];
end

if nargin < 2 || isempty(bands)
    bands = ["f0_4Hz", "f4_8Hz", "f8_13Hz", "f13_30Hz", "f30_45Hz"];
end

if nargin < 3 || isempty(metrics)
    metrics = ["clustering", "characteristic_path_length", "global_efficiency", ...
    "modularity", "modules", "centrality", "modular_span", ...
    "participation_coefficient", "degree", "wpli", "wpli_array"];
end

queries = {{'chennu', conf_types, bands, metrics}};

end