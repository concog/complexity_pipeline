function data = reformat(data, nsubjects, nfeatures, ntimepoints)

if nargin < 2
    % From 3D to 2D
    [nsubjects, nfeatures, ntimepoints] = size(data);
    data = permute(data, [1 3 2]); 
    data = reshape(data, nsubjects*ntimepoints, nfeatures);
else
    % From 2D to 3D
    data = reshape(data, nsubjects, ntimepoints, nfeatures);
    data = permute(data, [1 3 2]);
end

end