function fig = correlation_plot(features1, features2, queries1, queries2, varargin)

% Define default parameters and parse varargin
params = struct('correlation_type', 'pearson', 'remove_outliers', false, 'xticklabels', [], 'xtickangle', 45);
params = parse_varargin(params, varargin{:});

if params.remove_outliers
    o1 = isoutlier(features1, 'quartiles');
    o2 = isoutlier(features2, 'quartiles');
    features1(o1) = nan;
    features2(o2) = nan;
end

% Calculate correlations
[all_r, all_p] = corr([features1, features2], 'rows', 'pairwise', 'type', params.correlation_type);

nfeatures1 = size(features1, 2);
r = all_r(nfeatures1+1:end, 1:nfeatures1);
p = all_p(nfeatures1+1:end, 1:nfeatures1);

% Do FDR correction
h_bh = fdr_bh(p);
h_bonf = p < 0.05/numel(r);
h = p < 0.05;

fig = figure(); hold on;

% Plot significant correlations
sig = scatter(find(h), r(find(h)), 100, 'o', 'markerfacecolor', 'r', 'markeredgecolor', 'r', 'markerfacealpha', 1);
sig_bh = scatter(find(h_bh), r(find(h_bh)), 100, 'o', 'markerfacecolor', 'm', 'markeredgecolor', 'm', 'markerfacealpha', 1);
sig_bonf = scatter(find(h_bonf), r(find(h_bonf)), 100, 'o', 'markerfacecolor', 'k', 'markeredgecolor', 'k', 'markerfacealpha', 1);

% Plot non-significant correlations
non_sig = scatter(find(~h), r(find(~h)), 100, 'o', 'markerfacealpha', 0, 'markeredgecolor', 'k');

% Plot zero-line
plot([0, numel(h)+1], [0, 0], 'k--', 'linewidth', 2);

% Legend and labels
legend([sig_bonf, sig_bh, sig, non_sig], ["Bonferroni significant", "FDR-BH significant", "p < 0.05", "non-significant"], 'location', 'best', 'interpreter', 'none');
ylabel('Correlation coefficient R');

% Make sure to include the correct queries; use feature names as
% xticklabels
if nfeatures1 == 1
    queries = parse_queries(queries2);
    target = strjoin(string(queries1{1}), ' / ');
else
    queries = parse_queries(queries1);
    target = strjoin(string(queries2{1}), ' / ');
end

% Title and xticklabels
title({'Correlations with', target}, 'interpreter', 'none');
if ~isempty(params.xticklabels)
    % Make sure there's correct number of labels
    if numel(params.xticklabels) ~= numel(queries)
        warning('Incorrect number of xticklabels provided, will not use them');
    else
        xticks(1:numel(params.xticklabels));
        xticklabels(params.xticklabels);
        set(gca, 'TickLabelInterpreter', 'none');
        xtickangle(params.xtickangle);
    end
else
    xlabel('Feature');
end

hdt = datacursormode;
set(hdt,'UpdateFcn',{@labeltips1d, queries, 'R'},'Interpreter','none')

end


function set_xticklabels(queries)
% We'd like to have queries as xticklabels, but they are quite long
% sometimes. Let's get rid of all unnecessary qualifiers (like 'locations')
labels = strings(numel(queries),1);
for query_idx = 1:numel(queries)
    query = queries{query_idx};
    if query{1} == "locations"
        labels(query_idx) = strjoin(query(2:end), ' / ');
    else
        labels(query_idx) = strjoin(query, ' / ');
    end
end

xticks(1:numel(queries));
xticklabels(labels);
set(gca, 'TickLabelInterpreter','none');
xtickangle(90);

end