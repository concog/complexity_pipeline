classdef ComplexityParams
    %UNTITLED Parameters for analysis of ComplexityDataSets
    
    properties
        groups
        timepoints
        remove_outliers
        format
    end
    
    methods
        function obj = ComplexityParams(cdataset, varargin)
            %ComplexityParams Construct an instance of this class
            %   Detailed explanation goes here
            
            error('Work in progress; do not use');
            
            if nargin > 0 && mod(nargin, 2) ~= 0
                error('Input parameters are given in key-value pairs, thus even number of inputs is expected');
            end

            % Default parameters
            obj.remove_outliers = false;
            obj.format = "cell";
            obj.timepoints = nan;
            
            % Set given parameters
            for k = 1:2:nargin
                key = varargin{k};
                if ~isprop(obj, key)
                    warning('Parameter [%s] is not defined', key);
                    continue;
                end                
                obj.(key) = varargin{k+1};
            end
        end
        
        function outputArg = method1(obj,inputArg)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            outputArg = obj.Property1 + inputArg;
        end
    end
end

