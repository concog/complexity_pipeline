classdef ComplexityDataSet
    %UNTITLED8 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        subjects
        nsubjects
        features % List of features this cdataset contains
        nfeatures
        python_script
        name
        info
    end
    
    methods
        function obj = ComplexityDataSet(dataset, queries)
            
            % Go through processed files and load all of them
            files = dataset.get_processed_files;

            % Initialise par_subjects with empty ComplexitySubjects
            par_subjects(1:dataset.nsubjects) = ComplexitySubject;
            
            % Read num_good_epochs query by default so we can do weighted
            % averages
            if nargin < 2 || (numel(queries)==1 && isempty(queries{1}))
                queries = {{'num_good_epochs'}};
            else
                queries(end+1) = {{'num_good_epochs'}};
            end
            
            % Load data corresponding to queries
            parfor subject_idx = 1:dataset.nsubjects
                par_subjects(subject_idx) = ComplexitySubject(files(subject_idx), queries);
            end
            
            % If pipeline hasn't finished for a subject for whatever reason
            % (pipeline crashed or user stopped it), don't use those
            % subjects -- they might be lacking some features
            retain_subjects = [par_subjects.valid];

            % Set subjects into this object
            obj.subjects = par_subjects(retain_subjects);
            obj.nsubjects = sum(retain_subjects);

            % Update feature info
            obj = update_features(obj);

            % We might want to know which python file was used to calculate
            % the files in this ComplexityDataSet
            obj.python_script = dataset.python_script;

            % Save the name
            obj.name = dataset.name;
            
            % Actually save the whole dataset
            obj.info = dataset;
        end
        
        function obj = filter_subjects(obj, retain)
            % Filter out subjects; return the dataset with only 'retain' (a
            % logical vector) subjects
            obj.subjects(~retain) = [];
            obj.nsubjects = numel(obj.subjects);
        end
        
        function missing = missing_features(obj, queries)
            % Returns queries that have not been loaded
            
            % Parse queries
            parsed_queries = parse_queries(queries);
            
            missing = [];
            
            for query_idx = 1:numel(parsed_queries)
                query = parsed_queries(query_idx);
                if all(obj.features ~= Feature.get_name(query{1}))
                    missing = [missing, query];
                end
            end
        end
        
        function [data, parsed_queries] = get_data(obj, queries, varargin)
            % Returns data corresponding to given queries and parameters
            %
            % Optional inputs are
            %   - 'subjects' a logical vector or vector of indices that
            %   indicates for which subjects the data is returned
            %   - 'format' can be 'medians' in which case median over
            %   timepoints is returned for features, or 'cell' in which
            %   case features are returned in cells for each subject
            %   - 'timepoints' which is a string vector of timepoints -- if
            %   the first element in this vector is "except", then all
            %   other timepoints are returned except the ones following
            %   that keyword
            %
            % NOTE!! Currently there is no easy way of returning the
            % timepoints available for a feature. You can look up
            % obj.subjects(1).features(1).timepoints_lookup to retrieve the
            % timepoints for whatever feature features(1) is
            
            % If features have equal dimensions they are concatenated into
            % one big array, in which case the returned array's dimensions
            % are nsubjects x nfeatures x ntimepoints (x feature
            % dimensions). If returned features have different numbers of
            % dimensions this function returns multiple arrays, and those
            % arrays have dimensions nsubjects x ntimepoints (x feature
            % dimensions)

            % Parse queries
            parsed_queries = parse_queries(queries);
            
            % Check that queried features are loaded; if not, attempt
            % to load them
            missing_features = obj.missing_features(queries);
            if ~isempty(missing_features)
                str = strjoin(cellfun(@(x) strjoin(x, ' / '), missing_features), '\n');
                warning('Data corresponding to features\n\n%s\n\nare not loaded, will attempt to load them now -- might take some time', str);
                obj = obj.add_features(missing_features);
            end
            
            % Parse varargin parameters
            params = parse_varargin(...
                struct('subjects', (1:obj.nsubjects)', 'format', 'none', 'timepoints', []), ...
                varargin{:});
            
            % Check if only part of subjects are used
            if islogical(params.subjects)
                subject_idxs = find(params.subjects);
            else
                subject_idxs = params.subjects;
            end
            
            % Sanity check: if no subjects are queried return an empty
            % array
            if isempty(subject_idxs)
                data = [];
                return;
            end
            
            % Get data from each queried subject
            data_feature_sets = cell(numel(subject_idxs),1);
            parfor k = 1:numel(subject_idxs)
                subject_idx = subject_idxs(k);
                data_feature_sets{k} = obj.subjects(subject_idx).get_data(parsed_queries, params.timepoints);
            end

            % Format data
            
            % Transform nested cells into a cell array if necessary
            if iscell(data_feature_sets{1})
                data_feature_sets = vertcat(data_feature_sets{:});
            end
            
            % Warn if there are different number of timepoints in
            % features
            for feature_set_idx = 1:size(data_feature_sets,2)
                ntimepoints = cellfun(@(x) size(x,2), data_feature_sets(:,feature_set_idx));
                utimepoints = unique(ntimepoints);
                if numel(utimepoints) > 1
                    warning_msg = sprintf('Number of timepoints vary in returned features:\n');
                    for tp_idx = 1:numel(utimepoints)
                        warning_msg = sprintf('%s  - %d timepoints in %d features\n', ...
                            warning_msg, utimepoints(tp_idx), sum(ntimepoints==utimepoints(tp_idx)));
                    end
                    warning(warning_msg);
                end
            end
            
            % Take medians over timepoints if so required
            if any(params.format == "medians")

                % If multiple feature sets (i.e. separate features) are
                % returned, the first dimension contains timepoints
                if size(data_feature_sets,2) > 1
                    timepoint_dim = 1;
                else
                    timepoint_dim = 2;
                end
                
                % Calculate medians across timepoints
                data_feature_sets = cellfun(@(x) squeeze(nanmedian(x,timepoint_dim)), data_feature_sets, 'uniformoutput', false);

            elseif any(params.format == "weighted_means")

                % First we need to get numbers of good epochs per timepoint
                num_good_epochs = obj.get_data({{'num_good_epochs'}}, 'timepoints', params.timepoints);
                
                % Calculate proportion of epochs per timepoint for each
                % subject (makes it easier to calculate weighted average
                % later on)
                proportions = (num_good_epochs./sum(num_good_epochs,2))';
                
                % If multiple feature sets (i.e. separate features) are
                % returned, the first dimension contains timepoints
                if size(data_feature_sets,2) > 1
                    timepoint_dim = 1;
                else
                    timepoint_dim = 2;
                end
                               
                % Calculate weighted means only for those features that
                % have equal number of timepoints; we've already checked
                % that each feature set has equal number of timepoints, so
                % just grab the dimension from first subject's features
                for feature_set_idx = 1:size(data_feature_sets,2)
                    if isequal(size(data_feature_sets{1,feature_set_idx}, timepoint_dim), size(num_good_epochs,2))
                        % Calculate weighted mean and put them back into
                        % cells
                        if timepoint_dim == 1
                            cat_features = cat(2, data_feature_sets{:,feature_set_idx});
                            data_feature_sets(:, feature_set_idx) = num2cell(sum(cat_features .* proportions));
                        elseif timepoint_dim == 2
                            for subject_idx = 1:size(data_feature_sets,1)
                                data_feature_sets{subject_idx, feature_set_idx} = ...
                                    data_feature_sets{subject_idx, feature_set_idx} * proportions(:,subject_idx);
                            end
                        else
                            error('Unknown timepoint dimension %g (should be 1 or 2)', timepoint_dim);
                        end
                    end
                end

            end
            
            if any(params.format == "cells")
                % Return data as cells (one cell per subject)
                data = data_feature_sets;
            else
                % Concatenate returned features into arrays
                data = cell(1, size(data_feature_sets,2));
                for feature_set_idx = 1:size(data_feature_sets,2)

                    % Concatenate across subjects
                    subjects_dim = ndims(data_feature_sets{1}) + 1;
                    data{feature_set_idx} = cat(subjects_dim, data_feature_sets{:, 1});

                    % Permutate such that dimesions are 
                    % nsubjects x nfeatures x ntimepoints (x feature
                    % dimensions)
                    data{feature_set_idx} = permute(data{feature_set_idx}, [subjects_dim, 1:subjects_dim-1]);

                    % Try to save some memory by deleting the original
                    % data
                    data_feature_sets(:,1) = [];
                end

                % If there's only one cell, transform it into an array and
                % remove singleton dimensions
                if numel(data) == 1
                    data = squeeze(data{1});
                end
            end
        end
        
        function obj = add_features(obj, queries)
            % If a feature is not in this dataset, add it
            add_these_features = obj.missing_features(queries);
            
            if numel(add_these_features) > 0
                
                % Get subjects for parallel processing
                par_subjects = obj.subjects;

                % Load queries / features
                parfor subject_idx = 1:numel(par_subjects)
                    par_subjects(subject_idx) = load_queries(par_subjects(subject_idx), add_these_features);
                end

                % Set updated subjects back into this dataset
                obj.subjects = par_subjects;

                % Update features
                obj = update_features(obj);
            end
        end
        
        function obj = remove_features(obj, queries)
            error('Make sure this function works correctly after changing from struct features lookup to a string array');
            % Removes features specified in 'queries'
            rm_idxs = [];
            
            % Parse queries
            parsed_queries = parse_queries(queries);
            
            % Find indices corresponding to queries
            for query_idx = 1:numel(parsed_queries)
                query = parsed_queries(query_idx);
                feature_name = Feature.get_name(query{1});
                
                rm_idx = obj.features == feature_name;
                if any(rm_idx)
                    rm_idxs = [rm_idxs, find(rm_idx)];
                end
            end
            
            obj = remove_features_indices(obj, rm_idxs);
        end
        
        function obj = remove_features_indices(obj, rm)
            % Removes features specified in 'rm'; 'rm' can be a logical vector
            % indicating which features are removed, or a vector containing
            % feature indices
            
            if islogical(rm)
                % Transform logical vector into indices
                rm_idxs = find(rm);
            else
                rm_idxs = rm;
            end
            
            % If rm_idxs is empty don't remove anything
            if isempty(rm_idxs)
                warning('No features were removed');
                return;
            end
            
            if max(rm_idxs) > numel(obj.features) || min(rm_idxs) < 1
                error('Invalid feature indices encountered: indices must be between 1 and %d', numel(obj.features));
            end

            
            % Loop through subjects and remove features
            for subject_idx = 1:obj.nsubjects
                obj.subjects(subject_idx) = obj.subjects(subject_idx).remove_features_indices(rm_idxs);
            end
            
            % Update features
            obj = update_features(obj);
        end
    end
    
    methods (Access = private)        
        function obj = update_features(obj)
            
            % Update feature names, number of features, and the look-up
            % table
            obj.features = obj.subjects(1).features_lookup;
            obj.nfeatures = numel(obj.features);
            %obj.features_lookup = obj.subjects(1).features_lookup;
        end
    end
end

