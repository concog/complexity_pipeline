classdef ComplexitySubject
    % ComplexitySubject Read and manipulate processed results of 
    % complexity_pipeline
    
    properties
        file_processed % File containing output of complexity_pipeline
        
        features 
        features_lookup % Look-up table for features
        nfeatures
                
        valid = true % If pipeline has not finished mark subject as invalid
    end
    
    methods
        function obj = ComplexitySubject(file_processed, queries)
            
            % Create an empty object when no inputs are given; this is
            % necessary in order to make parfor-loops work with these
            % objects
            if nargin < 2
                return;
            end
            
            % Save this subject's filename
            obj.file_processed = file_processed;
   
            % Load the queries from the file
            obj = load_queries(obj, queries);
        end
        
        function obj = load_queries(obj, queries)

            % Open the processed data file
            json = fileread(char(obj.file_processed));
            d = jsondecode(json);
            
            % If pipeline is not finished, don't use this subject
            if ~isfield(d.info, 'pipeline_finished')
                obj.valid = false;
                return;
            end
                        
            % Parse queries
            parsed_queries = parse_queries(queries);
            
            % Initialise obj.features
            if isempty(obj.nfeatures)
                % There are no previous features
                existing_nfeatures = 0;
                
                % Initialise some dummy features (place holders), not
                % really sure if this helps?
                obj.features = ConstantFeature.empty;
                obj.features(1:numel(parsed_queries)) = ConstantFeature("dummy", nan);
                
                % Initialise lookup-table
                obj.features_lookup = strings(numel(parsed_queries), 1);
            else
                % Get number of existing features
                existing_nfeatures = obj.nfeatures;
                
                % Initialise some dummy features (place holders)
                obj.features(obj.nfeatures+1:obj.nfeatures+numel(parsed_queries)) = ConstantFeature("dummy", nan);
                
                % Initialise some more space into lookup-table
                obj.features_lookup(obj.nfeatures+1:obj.nfeatures+numel(parsed_queries)) = "";
            end

            % Update number of features
            obj.nfeatures = numel(obj.features);
            
            for query_idx = 1:numel(parsed_queries)
                % Current query
                cquery = parsed_queries{query_idx};

                % Get the complexity data
                query_data = getfield(d, cquery{1:end-1}, cquery{end});
 
                % Create a Feature and set queried data into it
                feature_idx = existing_nfeatures + query_idx;
                if isstruct(query_data)
                    obj.features(feature_idx) = VariableFeature(cquery, query_data);
                else
                    if ischar(query_data)
                        query_data = string(query_data);
                    end
                    obj.features(feature_idx) = ConstantFeature(cquery, query_data);
                end
                
                obj.features_lookup(feature_idx) = obj.features(feature_idx).name;
            end
        end
        
        function [qfeatures, qnames] = get_features(obj, queries)
            parsed_queries = parse_queries(queries);
            
            qfeatures(1:numel(parsed_queries)) = ConstantFeature("dummy", nan);
            qnames = cell(numel(parsed_queries),1);
            
            for query_idx = 1:numel(parsed_queries)
                feature_name = Feature.get_name(parsed_queries{query_idx});
                idx = obj.features_lookup == feature_name;
                if sum(idx) == 0
                    error('Feature %s was not found', feature_name);
                end
                qnames(query_idx) = parsed_queries(query_idx);
                qfeatures(query_idx) = obj.features(idx);
            end
        end
        
        function data = get_data(obj, queries, timepoints)
            qfeatures = get_features(obj, queries);
            
            % If no timepoints are provided, use all
            if nargin < 3
                timepoints = [];
            end

            % Collect the features' data into a cell
            data = cell(1, numel(qfeatures));
            for feature_idx = 1:numel(qfeatures)
                data{feature_idx} = qfeatures(feature_idx).get_data(timepoints);
            end                
            
            % If we're retrieving data with equal dimensions concatenate
            % them into an array
            siz = size(data{1});
            if all(cellfun(@(x) ndims(x)==numel(siz) && all(size(x)==siz), data))
                feature_dim = ndims(data{1})+1;
                data = cat(feature_dim, data{:});
                data = permute(data, [feature_dim, 1:feature_dim-1]);
            end
        end
                
        function obj = remove_features(obj, queries)
            error('Make sure this function works correctly after changing from struct features lookup to a string array');
            parsed_queries = parse_queries(queries);
            
            % Collect features here that will be removed
            rm_idxs = [];
            
            for query_idx = 1:numel(parsed_queries)
                % Get current query
                query = parsed_queries{query_idx};
                
                % Get feature name that corresponds to this query
                feature_name = Feature.get_name(query);
                
                % If this feature doesn't exist, continue to the next one
                feature_idx = obj.features_lookup == feature_name;
                if ~any(feature_idx)
                    continue;
                end
                               
                % Mark features for remove
                rm_idxs = [rm_idxs, find(feature_idx)];
                                
            end

            % Remove the features
            obj = remove_features_indices(obj, rm_idxs);
        end
        
        function obj = remove_features_indices(obj, rm)
            % Remove features with indices 'rm', which can be either a
            % logical or a numeric vector
            
            if islogical(rm)
                rm_idxs = find(rm);
            else
                if max(rm) > obj.nfeatures || min(rm) < 1
                    error('Invalid feature indices encountered: indices must be between 1 and %d', numel(obj.features));
                end
                rm_idxs = rm;
            end
            
            % Remove the features
            obj.features(rm_idxs) = [];

            % Update nfeatures
            obj.nfeatures = numel(obj.features);

            % We need to also update indices of features in the look-up
            % table
            obj.features_lookup(rm_idxs) = [];
            
        end
    end
    
end

