classdef ConstantFeature < Feature
    %UNTITLED6 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function obj = ConstantFeature(query, value)
            % Set the feature name
            obj.name = Feature.get_name(query);
            
            if isempty(value)
                value = nan;
            end
            
            % Set the value -- first dimension should be timepoint
            % dimension, which is one in constant features
            obj.data = shiftdim(value, -1);
        end
        
        function value = get_data(obj, timepoints)            
            % Get the value
            value = obj.data;
        end
    end
    
end

