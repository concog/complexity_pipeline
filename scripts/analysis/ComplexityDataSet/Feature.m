classdef (Abstract) Feature < matlab.mixin.Heterogeneous
    %UNTITLED4 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        name
        data  % note: you should use get_data to retrieve this variable
    end
        
    methods (Abstract)
        value = get_data(obj, timepoints)
    end
    
    methods (Static)
        function feature_name = get_name(query)
            feature_name = char(strjoin(query, ' / '));
        end
    end
    
    methods (Static, Sealed, Access = protected)
        function default_object = getDefaultScalarElement
            default_object = ConstantFeature("dummy", nan);
        end
    end
end

