classdef VariableFeature < Feature
    %VARIABLEFEATURE Contains data from multiple timepoints
    
    properties
        timepoints_lookup
    end
    
    methods
        
        function obj = VariableFeature(query, timepoints)
            
            % Set feature name
            obj.name = Feature.get_name(query);
            
            % Get names of timepoints
            fields = fieldnames(timepoints);
            
            % Initialise timepoints lookup-table
            obj.timepoints_lookup = sort_nat(fields);
           
            % Loop through timepoints and set the values into obj.data
            obj.data = cell(1, numel(fields));
            for timepoint_idx = 1:numel(fields)
                % Get value for this timepoint
                value = timepoints.(obj.timepoints_lookup{timepoint_idx});

                % Make value nan if it's empty
                if isempty(value)
                    value = nan;
                end
                
                % Set the value into this object
                obj.data{timepoint_idx} = value;
            end
            
            % Set timepoints as the first dimension (need to concatenate
            % data first with timepoints as the last dimension though)
            if isscalar(obj.data{1})
                timepoint_dim = 2;
            else
                timepoint_dim = ndims(obj.data{1}) + 1;
            end
            
            obj.data = cat(timepoint_dim, obj.data{:});
            obj.data = permute(obj.data, [timepoint_dim, 1:timepoint_dim-1]);
            
        end
        
        function values = get_data(obj, timepoints)
            % Return the data
            
            if isempty(timepoints)
                
                % Return all timepoints
                values = obj.data;
                
            else
                
                % Parse timepoints into a numeric vector
                if islogical(timepoints)
                    timepoints = find(timepoints);
                elseif isstring(timepoints) || ischar(timepoints)
                    % Make sure timepoints are strings
                    timepoints = string(timepoints);
                    
                    % If first key is "except" return all timepoints except
                    % the ones that are specified after that key
                    if timepoints(1) == "except"
                        ignored = arrayfun(@(x) find(x==obj.timepoints_lookup), timepoints(2:end));
                        timepoints = 1:numel(obj.timepoints_lookup);
                        timepoints(ignored) = [];
                    else
                        timepoints = arrayfun(@(x) find(x==obj.timepoints_lookup), timepoints);
                    end
                end
                
                % Check indices are inside boundaries
                if min(timepoints) < 1 || max(timepoints) > numel(obj.timepoints_lookup)
                    error('Timepoints outside boundaries; check your timepoints');
                end
                
                % Return data
                otherdims = repmat({':'},1,ndims(obj.data)-1);
                values = obj.data(timepoints, otherdims{:});
            end
        end
        
    end
    
end

