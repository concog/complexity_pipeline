function predicted = predict_measure(mdl, features)
%PREDICT Summary of this function goes here

% Reformat features into a 2D array
[nsubjects, ~, ntimepoints] = size(features);
features = reformat(features);

% Predict values with the model
predicted = mdl.predict(features);

% Reformat the predicted values -- one prediction per subject per timepoint
predicted = reformat(predicted, nsubjects, 1, ntimepoints);

end
