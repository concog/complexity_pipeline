function [fig1, fig2, r, p, queries1, queries2] = correlation_matrix(features1, features2, queries1, queries2, varargin)
% CORRELATION_MATRIX Correlate queries1 against queries2

% Default parameters
params = struct( ...
    'remove_outliers', false, ...
    'reorder', 0, ...
    'remove_features', 0, ...
    'correlation_type', 'pearson', ...
    'plot_type', 'imagesc', ...
    'xticklabels', [], ...
    'xtickangle', 45 ...
    );

params = parse_varargin(params, varargin{:});

if params.remove_outliers
    o1 = isoutlier(features1, 'quartiles');
    o2 = isoutlier(features2, 'quartiles');
    features1(o1) = nan;
    features2(o2) = nan;
end

[all_r, all_p] = corr([features1, features2], 'type', params.correlation_type, 'rows', 'pairwise');

nfeatures1 = size(features1, 2);
r = all_r(nfeatures1+1:end, 1:nfeatures1);
p = all_p(nfeatures1+1:end, 1:nfeatures1);

% Figure out which correlations are FDR-BH significant
h = fdr_bh(p);

% Remove rows/columns
if params.remove_features
    [r, h, queries1, queries2] = remove_rows_correlation_matrix(r, h, queries1, queries2, params.remove_features);
end

% Reorder rows/columns
if params.reorder
    [r, h, queries1, queries2] = reorder_correlation_matrix(r, h, queries1, queries2, 'threshold', params.reorder);
end

% Set non-significant correlations as nans
rt = r;
rt(~h) = nan;

if params.plot_type == "imagesc"
    
    % Plot first all correlations
    fig1 = figure(); 
    imagesc(r);
    
    % Set xticklabels if given
    xticks(1:size(r,2));
    if ~isempty(params.xticklabels)
        xticklabels(params.xticklabels);
        xtickangle(params.xtickangle);
    end
    
    % Make sure colorbar is visible
    colorbar
    
    % Set query names for data cursor
    if nargin > 2 && ~isempty(queries1) && ~isempty(queries2)
        queries1 = parse_queries(queries1);
        queries2 = parse_queries(queries2);

        hdt = datacursormode;
        set(hdt,'UpdateFcn',{@labeltips2d, r, queries1, queries2},'Interpreter','none')
    end
    
    % Then plot only significant correlations
    fig2 = figure(); 
    imagesc(rt, 'alphadata', ~isnan(rt));
    
    % Set xticklabels if given
    xticks(1:size(r,2));
    if ~isempty(params.xticklabels)
        xticklabels(params.xticklabels);
        xtickangle(params.xtickangle);
    end
    
    % Make sure colorbar is visible
    colorbar
    
    % Set queries for data cursor
    if nargin > 2 && ~isempty(queries1) && ~isempty(queries2)
        hdt = datacursormode;
        set(hdt,'UpdateFcn',{@labeltips2d, rt, queries1, queries2},'Interpreter','none')
    end


elseif params.plot_type == "bar"
    
    % Check if xticklabels were given
    if ~isempty(params.xticklabels)
        xlabels = params.xticklabels;
    else
        % Check if there's only one part of the query that's changing. If so,
        % use that to plot tick labels, otherwise use complete queries
        [val,idx] = max(cellfun(@(x) numel(string(x)), queries1{1}));
        if val > 1
            xlabels = cellfun(@(x) strjoin(x, ' / '), parse_queries({queries1{1}(2:end)}));
        else
            xlabels = cellfun(@(x) strjoin(x, ' / '), parse_queries(queries1));
        end
    end
    
    % Plot first all correlations
    fig1 = figure('Renderer', 'painters', 'Position', [10 10 1000 800]);
    bar(r');
    legend(cellfun(@(x) strjoin(x,' / '), parse_queries(queries2)), 'interpreter', 'none', 'location', 'best')
    ylabel(sprintf('%s correlation coefficient', params.correlation_type));
    xticklabels(xlabels);
    set(gca, 'TickLabelInterpreter', 'none');
    xtickangle(params.xtickangle);
    
    % Then plot only significant correlations
    fig2 = figure('Position', [10 10 1000 800]);
    bar(rt');
    legend(cellfun(@(x) strjoin(x,' / '), parse_queries(queries2)), 'interpreter', 'none', 'location', 'best')
    ylabel(sprintf('%s correlation coefficient', params.correlation_type));
    xticklabels(xlabels);
    set(gca, 'TickLabelInterpreter', 'none');
    xtickangle(params.xtickangle);

end


end
