function fig = correlation_wrapper(dataset, queries1, queries2, varargin)

features1 = dataset.get_data(queries1, 'format', 'medians');
features2 = dataset.get_data(queries2, 'format', 'medians');

if size(features1, 2) > 1 && size(features2, 2) > 1
    fig = correlation_matrix(features1, features2, queries1, queries2, varargin{:});
else
    fig = correlation_plot(features1, features2, queries1, queries2, varargin{:});
end

end