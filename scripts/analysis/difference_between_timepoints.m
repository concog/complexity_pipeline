function fig2 = difference_between_timepoints(cdataset, queries, first_timepoint, second_timepoint)
%DIFFERENCE_BETWEEN_TIMEPOINTS Difference between timepoints
%   Assumes first_timepoint is before (chronologically) second_timepoint

% If timepoints are not defined use first and last
if nargin < 3
    first_timepoint = 1;
    second_timepoint = cdataset.ntimepoints;
end

% Transform timepoints into logical vectors
first_timepoint = make_logical(first_timepoint, cdataset.ntimepoints);
second_timepoint = make_logical(second_timepoint, cdataset.ntimepoints);

% Make sure timepoints are not overlapping
if any(first_timepoint & second_timepoint)
    error('Overlapping timepoints');
end

% Make sure all features are loaded
cdataset = cdataset.add_features(queries);

% Get data
first_data = cdataset.get_data(queries, 'format', 'medians', 'timepoints', first_timepoint);
second_data = cdataset.get_data(queries, 'format', 'medians', 'timepoints', second_timepoint);

% Do a signrank test
dfrnc = first_data - second_data;
%[~, p] = ttest(first_timepoint, last_timepoint);
p = signrank_array(dfrnc);

% Do Benjamini-Hochberg FDR correction
[h, p_bh] = fdr_bh(p);

% Calculate median difference; actually, divide also by std so that
% they're roughly in the same scale
dfrnc = median(dfrnc);
%dfrnc = median(dfrnc)./std(dfrnc);

% Then do some plotting
%fig1 = get_figure(mfilename, 1); hold on;
%fig1 = figure(); hold on;

% Plot significants
%sig_h = scatter(find(h), dfrnc(find(h)), 100, 'o', 'MarkerFaceColor', 'k', 'markeredgecolor', 'k');

% Plot non-significants
%nonsig_h = scatter(find(~h), dfrnc(find(~h)), 100, 'o', 'markeredgecolor', 'k', 'markerfacealpha', 0);

% Plot zero line
%plot([0, numel(h)+1], [0, 0], 'k--');

% Set legends, titles, etc.
%legend([sig_h, nonsig_h], ["Significant difference", "Non-significant difference"], 'location', 'best');
%title('Difference between timepoints');
%xlabel('Feature'); ylabel('Difference');

%hdt = datacursormode;
%set(hdt,'UpdateFcn',{@labeltips1d, parse_queries(queries), 'Difference'})

% Manhattan style plot
%fig2 = get_figure([mfilename '_manhattan'], 1); hold on;
fig2 = figure(); hold on;

% Find positive differences
pos = dfrnc >= 0;
neg = dfrnc < 0;

% Plot negatives
scatter(find(neg), -log10(p(neg)), 100, '^', 'markerfacecolor', 'g', 'markeredgecolor', 'g');

% Plot positives
scatter(find(pos), -log10(p(pos)), 100, 'v', 'markerfacecolor', 'r', 'markeredgecolor', 'r');

% Plot Bonferroni threshold
plot([0, numel(h)+1], [-log10(0.05/numel(h)), -log10(0.05/numel(h))], 'k--', 'linewidth', 2);

% Plot Benjamini-Hochberg threshold
plot([0, numel(h)+1], [-log10(p_bh), -log10(p_bh)], 'm--', 'linewidth', 2);

% Titles and such
legend(["Increases during task", "Decreases during task", "Bonferroni", "Benjamini-Hochberg"], 'location', 'best');
title(sprintf('Difference between timepoints %s and %s', ...
    strjoin(string(find(first_timepoint)), ', '), strjoin(string(find(second_timepoint)), ', ')));
xlabel('Feature'); ylabel('-log10(p)');

hdt = datacursormode;
set(hdt,'UpdateFcn',{@labeltips1d, parse_queries(queries), 'p', @(x) 10^(-x)})

end

