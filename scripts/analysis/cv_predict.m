function perf = cv_predict(features, response, cv, reps)

% Check how many times we repeat this analysis
if reps == 1
    visualize = true;
else
    visualize = false;
end

% Define folds for training/testing
folds = k_folds(response, cv, reps);


% Loop through replications, cross-validate, train and predict
perf = cell(reps, 1);
parfor repeat_idx = 1:reps
    perf(repeat_idx) = cross_validate(features, response, folds{repeat_idx}, visualize);
end

%if isnumeric(response)
%    msg = 'R squared:';
%else
%    msg = 'Classification rate:';
%end

%prcs = prctile(perf, [5, 50, 95]);
%msg = sprintf('%s %f', msg, prcs(2));
%if reps > 2
%    msg = sprintf('%s (%f, %f)', msg, prcs(1), prcs(3));
%end

%fprintf('%s, with %d repetitions\n', msg, reps);

end


function perf = cross_validate(features, response, folds, visualize)

% Define yfit as a numeric or string vector depending on whether this is a
% regression or a classification task
if isnumeric(response)
    predictions = nan(size(response));
else
    predictions = strings(size(response));
end

predictions_folds = cell(numel(folds), 1);
    
for idx = 1:numel(folds)
    fold = folds(idx);

    % Dimension reduction?
    train_features = features(fold.train_indices, :, :);
    train_response = response(fold.train_indices, :, :);
    
    %nans = isnan(train_response) | sum(isnan(train_features),2)>0;
    %[~,~,~,~,~,pctvar,~,stats] = plsregress(zscore(train_features(~nans,:)), train_response(~nans), 15);
    
    %m = nanmean(train_features);
    %s = nanstd(train_features);
    %train_features = ((train_features-m)./s) * stats.W;
    
    test_features = features(fold.test_indices, :, :);
    %test_features = ((test_features-m)./s) * stats.W;
    
    % Train and predict
    mdl = train_model("svm", train_features, train_response);
    predictions_folds{idx} = predict_measure(mdl, test_features);
end

% Reconstruct yfit
for idx = 1:numel(folds)
    % If there's only one timepoint in response, take the median of
    % predictions per subject
    if size(response, 3) == 1
        pred = mean(predictions_folds{idx}, 3);
    else
        pred = predictions_folds{idx};
    end
    
    predictions(folds(idx).test_indices, :, :) = pred;
end

% Reformat response and predictions (in case they have multiple timepoints)
response = reformat(response);
predictions = reformat(predictions);

% Calculate performance metrics or plot / print some results
if isnumeric(response)

    idxs = ~isnan(response) & ~isnan(predictions);
    [r,p] = corr(predictions(idxs), response(idxs), 'type', 'spearman');
    
    % Calculate R squared from variance sum of squares
    ssres = sum((predictions(idxs)-response(idxs)).^2);
    sstot = sum((response(idxs)-mean(response(idxs))).^2);
    
    mad = mean(abs(response(idxs)-predictions(idxs)));
    fprintf('Mean absolute difference: %f\n', mad);

    perf = {[1 - (ssres/sstot), mad]};
    
    if visualize
        h = get_figure(mfilename,1);
        scatter(predictions(idxs), response(idxs));
        title(['Rsquared = ' num2str(perf{1}(1)) '; Spearman rho = ' num2str(r) ', p = ' num2str(p)],'Interpreter','none');
        ylabel('Actual values');
        xlabel('Predicted values');
    end
else
    perf = {sum(predictions==response)/numel(response)};
end

end