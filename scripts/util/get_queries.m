function queries = get_queries(type)

queries = {};

switch type
    case "group_channel"
        complexities = ["LZc", "LZc_col", "LZsum", "SSE", "ZSFD"];
        locs = get_locations('all_areas');
        freqs = get_freq_bands('broadband');
        queries{end+1} = {'locations', locs, [get_freq_bands('broadband'), get_freq_bands('2hz_bins')], "power"};
        queries{end+1} = {'locations', locs, freqs, complexities};
        queries{end+1} = {'locations', locs, freqs, "MSE", ["scale_1", "scale_2", "scale_3", "scale_4", "scale_5", "scale_6", "scale_7", "scale_8", "scale_9", "scale_10"]};
        queries{end+1} = {'locations', "glob", get_freq_bands('ple'), "PLE", "averages", ["odd_even", "distributed_odd_even"]};

    case "single_channel"
        complexities = ["LZc", "LZsum", "SSE", "ZSFD"];
        locs = get_locations('channels');
        freqs = get_freq_bands('broadband');
        queries{end+1} = {'locations', locs, [get_freq_bands('broadband'), get_freq_bands('2hz_bins')], "power"};
        queries{end+1} = {'locations', locs, freqs, complexities};
        queries{end+1} = {'locations', locs, freqs, "MSE", ["scale_1", "scale_2", "scale_3", "scale_4", "scale_5", "scale_6", "scale_7", "scale_8", "scale_9", "scale_10"]};

    otherwise
        error('No queries defined for type ''%s''', type);
end

end