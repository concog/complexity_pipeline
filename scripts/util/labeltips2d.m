function output_txt = labeltips2d(obj, event_obj, value_matrix, xqueries, yqueries)

pos = get(event_obj,'Position');
x = pos(1); y = pos(2);

xquery = strjoin(xqueries{x}, ' / ');
yquery = strjoin(yqueries{y}, ' / ');

output_txt = {...
    ['(' num2str(x) ') ' char(xquery)], ...
    '', ...
    ['r = ' num2str(value_matrix(y, x))], ...
    '', ...
    ['(' num2str(y) ') ' char(yquery)]
    };

end