function output_txt = labeltips1d(obj, event_obj, queries, y_label, y_value_conv_fun)

if nargin < 4
    y_label = 'y';
end

if nargin < 5
    y_value_conv_fun = @(x) x;
end

pos = get(event_obj,'Position');
x = pos(1); y = pos(2);

query = strjoin(queries{x}, ' / ');

output_txt = {...
    ['(' num2str(x) ') ' char(query)], ...
    '', ...
    [y_label ' = ' num2str(y_value_conv_fun(y))]
    };
end