function [r, h, queries1, queries2] = remove_rows_correlation_matrix(r, h, queries1, queries2, threshold)

if nargin < 5
    threshold = 0.9;
end

rm1 = sum(h==0,1) > threshold*size(r,1);
rm2 = sum(h==0,2) > threshold*size(r,2);

r(:, rm1) = [];
r(rm2, :) = [];

h(:, rm1) = [];
h(rm2, :) = [];

if nargout > 2 && ~isempty(queries1) && ~isempty(queries2)
    queries1 = parse_queries(queries1);
    queries2 = parse_queries(queries2);
    queries1(rm1) = [];
    queries2(rm2) = [];
end
end