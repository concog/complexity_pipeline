function mask = create_feature_mask(features, targets)

features = cellfun(@(x) Feature.get_name(x), features, 'uniformoutput', false);
targets = cellfun(@(x) Feature.get_name(x), targets, 'uniformoutput', false);

mask = ismember(features, targets);

end