function params = parse_inputs(cdataset, varargin)

if nargin > 1 && isstruct(varargin{1})
    % Use given params as a starting point
    params = varargin{1};
    expected_mod = 0;
    start_idx = 2;
    end_idx = nargin-2;
else
    % Default parameters
    params = struct();
    params.format = "cell";
    params.subjects = true(cdataset.nsubjects, 1);
%    params.timepoints = true(cdataset.ntimepoints, 1);
    expected_mod = 1;
    start_idx = 1;
    end_idx = nargin-1;
end

if mod(nargin, 2) ~= expected_mod
    error('Input parameters are given in key-value pairs, thus even number of varargin inputs is expected');
end

% Set given parameters
for k = start_idx:2:end_idx
    key = varargin{k};
    switch string(key)
        %case "timepoints"
        %    if isempty(params.timepoints)
        %        warning('Tried to select timepoints even though this dataset has variable timepoints; param ignored');
        %        break;
        %    end
        %    params = set_timepoints(params, varargin{k+1});
       
        case "subjects"
            params = set_subjects(params, varargin{k+1});
            
        otherwise
            params.(varargin{k}) = varargin{k+1};
    end
end
end

function params = set_timepoints(params, timepoints)

if islogical(timepoints)
    if size(timepoints, 1) == 1
        timepoints = timepoints';
    end
    
    if any(size(params.timepoints) ~= size(timepoints))
        error('Timepoints must be a logical vector with %d elements', numel(params.timepoints));
    end
    params.timepoints = timepoints;

elseif isvector(timepoints) && all(~mod(timepoints,1)) % checks if the values are integers
    if min(timepoints) < 1 || max(timepoints) > numel(params.timepoints)
        error('There are only %d timepoints in this dataset', numel(params.timepoints))
    end
    
    params.timepoints = false(size(params.timepoints));
    params.timepoints(timepoints) = true;
else
    error('Timepoints must be a logical or integer vector')
end
end

function params = set_subjects(params, subjects)

if islogical(subjects)
    if size(subjects, 1) == 1
        subjects = subjects';
    end
    
    if any(size(params.subjects) ~= size(subjects))
        error('Subjects must be a logical vector with %d elements', numel(params.subjects));
    end
    params.subjects = subjects;

elseif isvector(subjects) && all(~mod(subjects,1)) % checks if the values are integers
    if min(subjects) < 1 || max(subjects) > numel(params.subjects)
        error('There are only %d subjects in this dataset', numel(params.subjects))
    end
    
    params.subjects = false(size(params.subjects));
    params.subjects(subjects) = true;
else
    error('Subjects must be a logical or integer vector')
end
end