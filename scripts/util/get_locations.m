function locs = get_locations(locs)

switch locs
    case "channels"
        locs = strings(204,1);
        for channel_idx = 1:204
            locs(channel_idx) = ['channel_' num2str(channel_idx)];
        end
    
    case "areas"
        locs = ["frontal", "left_frontal", "right_frontal", "occipital"];
        
    case "all_areas"
        locs = ["frontal", "left_frontal", "right_frontal", "occipital", "glob"];

    otherwise
        error('Input parameter ''locs'' must be either ''channels'' or ''areas''');
        
end

end