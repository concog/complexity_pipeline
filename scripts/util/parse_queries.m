function parsed_queries = parse_queries(queries)

parsed_queries = {};
for query_idx = 1:length(queries)
    query = queries{query_idx};
    parsed_queries = parse_sub_queries(query, parsed_queries);
end

end

function parsed_queries = parse_sub_queries(query, parsed_queries)
% Depth-first parsing, parse all elements in query until there are no more
% cells
for idx_query = 1:length(query)
    if iscell(query{idx_query}) || isstring(query{idx_query})
        for idx_subquery = 1:length(query{idx_query})
            subquery = query;
            subquery{idx_query} = query{idx_query}{idx_subquery};
            parsed_queries = parse_sub_queries(subquery, parsed_queries);
        end
        return;
    end
end

% Save this query so we can reuse it when looping through processed data
parsed_queries{end+1} = string(query);

end