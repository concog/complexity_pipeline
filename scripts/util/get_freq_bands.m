function bands = get_freq_bands(type)

if strcmp(type, 'broadband')
    bands = ...
        {
        'f3_5Hz', 'f8_12Hz', 'f13_30Hz', 'f0c5_30Hz'
        };
elseif lower(type) == "ple"
    bands = ...
        {
        'f3_5Hz', 'f8_12Hz', 'f13_30Hz', 'f0c5_30Hz', 'f0c5_8Hz', 'f8_30Hz'
        };
elseif strcmp(type, '2hz_bins')
    bands = ...
        [
        "f0_2Hz", "f2_4Hz", "f4_6Hz", "f6_8Hz", "f8_10Hz", ...
        "f10_12Hz", "f12_14Hz", "f14_16Hz", "f16_18Hz", "f18_20Hz", ...
        "f20_30Hz", "f30_40Hz", "f40_50Hz", "f50_60Hz", "f60_70Hz", ...
        "f70_80Hz", "f80_90Hz", "f90_100Hz"
        ];
else
    bands = ...
        {
        'f0c5_30Hz', 'f3_5Hz', 'f8_12Hz', 'f13_30Hz', 'f8_30Hz', ...
        'f0_2Hz', 'f2_4Hz', 'f4_6Hz', 'f6_8Hz', 'f8_10Hz', ...
        'f10_12Hz', 'f12_14Hz', 'f14_16Hz', 'f16_18Hz', 'f18_20Hz', ...
        'f20_22Hz', 'f22_24Hz', 'f24_26Hz', 'f26_28Hz', 'f28_30Hz'
        };
end

end