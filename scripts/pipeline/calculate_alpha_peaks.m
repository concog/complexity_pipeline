function alpha_peaks = calculate_alpha_peaks(data, fs, timepoints)
%addpath /home/dp01/matlab/lib
addpath /imaging/dp01/toolboxes/alphapeaks
addpath /home/ai04/Workspace/software/FastICA_25
addpath /home/ai04/Workspace/software/pca_ica
%loadspm 12

showplots = false;

location = struct();
confidence = struct();

% psds = struct();
occ_idxs = [145, 146, 159, 160, 177, 178, 131, 132, 161, 162, 163, 164, 195, 196];
%occ_idxs = [113,123,149,151,185,201,127,141,173,191,147,155,153,175,125,129,143,179,189,193,157,145,159,177,131,163,161,195];
%occ_idxs = [occ_idxs, occ_idxs+1];

% Calulate alpha peaks for each timepoint
timepoints_fields = fieldnames(timepoints);
for timepoint_idx = 1:length(timepoints_fields)
    timepoint = timepoints_fields{timepoint_idx};

    % Indexing starts from zero, need to add one to indices
    good_epochs = timepoints.(timepoint).epochs + 1;
    data_timepoint = data(occ_idxs, :, good_epochs);
    
    nsamples = size(data_timepoint,2);
    
    % Reshape data
    data_timepoint = reshape(data_timepoint, 14, nsamples*length(good_epochs));

    % fastica seems to be better in some cases, and kICA better in some
    icasig = kICA(data_timepoint,14);
    
    % Calculate power in alpha band
    [pxx,fxx] = pwelch(icasig',nsamples,0,[],fs);
    powers_alpha = bandpower(pxx, fxx, [8,12], 'psd');
    [~, idx] = max(powers_alpha);

    % Find the peaks
    F = ai04_findalphapeaks_return(icasig(idx,:), fs, showplots);

    % If fit is poor try again with fastica
    if F.confidence <= 0
        icasig = fastica(data_timepoint, 'approach','symm','g','tanh','numOfIC', 14, 'stabilization', 'on', 'epsilon', 1e-7, 'maxNumIterations', 1e4, 'verbose', 'off');

        % Calculate power in alpha band and frequencies > 12 Hz
        powers_alpha = bandpower(icasig', fs, [8,12]);
        [~, idx] = max(powers_alpha);

        % Find the peaks
        F_fica = ai04_findalphapeaks_return(icasig(idx,:), fs, showplots);
        
        % Check if this was better
       if F_fica.confidence > F.confidence
           F = F_fica;
       end
    end
    
    % Dimension reduction
%     Dpca = pca(ft_preproc_bandpassfilter(data_timepoint,fs,[5 15],  200, 'firls','twopass','reduce')');
%     data_timepoint = (data_timepoint'*Dpca)';
    % Reshape back to trials (just the first component)
%     data_timepoint = reshape(data_timepoint(1,:), 1, 1001, length(good_epochs));
%     data_timepoint = data_timepoint(1,:);
    
    location.(timepoint) = F.location;
    confidence.(timepoint) = F.confidence;
%     width = F.alpha(:,2);
%     power = F.alpha(:,3); % Power here has an arbitrary scaling
%     psds.(timepoint) = F.psd;
%     end
end

alpha_peaks = struct('location', location, 'confidence', confidence);

end