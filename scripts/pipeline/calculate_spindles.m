% Function to estimate spindles
%

function O = calculate_spindles(eeglab_filename, timepoints)

% We're using eeglab
addpath(genpath('/imaging/danielb/packages/eeglab13_2_2b/'))

[folder, file, ~] = fileparts(eeglab_filename);

EEG = pop_loadset('filename', [file '.set'], 'filepath', folder);

[~,misc] = process_classifyTrials_gradonly(EEG);
% O.tspindle = misc.tspindle;
% O.spindletrials = misc.spindletrials';

% Calculate spindles per minute
tspindle = [];
spindletrials = [];
timepoint_fields = fieldnames(timepoints);
for tp_idx = 1:length(timepoint_fields)
    timepoint = timepoint_fields{tp_idx};
    good_epochs = timepoints.(timepoint).epochs;
    tspindle.(timepoint) = sum(misc.tspindle(good_epochs+1));
    spindletrials.(timepoint) = sum(misc.spindletrials(good_epochs+1));
end

O = struct('tspindle', tspindle, 'spindletrials', spindletrials);


%Something in this path in eeglab clashes with the rest. Trying to remove.
rmpath /imaging/danielb/packages/eeglab13_2_2b/functions/octavefunc/signal;
rmpath /imaging/danielb/packages/eeglab13_2_2b/functions/octavefunc/;
rmpath /imaging/danielb/packages/eeglab13_2_2b/

end


% Shri's (Sridhar Jagannathan) alert/drowsy/deep drowsy trial classification
function [trials,misc] = process_classifyTrials_gradonly(EEG)

trials = [];
misc = [];
nelec = EEG.nbchan;

if (isfield(EEG,'rejepoch'))
    
    % compute nans for the rejected epochs..
    ntrials = EEG.trials + length(EEG.rejepoch);
    goodtrial = setdiff(1:324,EEG.rejepoch);
    
else
    
    ntrials = EEG.trials;
    goodtrial = 1:ntrials;
    
end


alphafreq = [9 11]; %Frequency range in Hz 9-11
thetafreq = [3 5]; %Frequency range in Hz 4-5
spindlefreq = [12 14]; %Frequency range in Hz 12-14

% 1. Initialize variables now..
[alpha,theta] = deal(nan(ntrials,nelec));
[varalpha,vartheta,varspindle]= deal(nan(nelec,ntrials));
[maxalpha,maxtheta,maxspindle] = deal(zeros(nelec,ntrials));
[alphatrials,thetatrials,spindletrials] = deal(zeros(1,ntrials));


% 2. Compute band power and variance explained..
%Changed this to only include gradiometer channels
%for k = 103:306
%for k = 1:306
for k = 1:nelec

 % The function to have a spectrum for one electrode
 [ersp,itc,powbase,times,freqs,erspboot,itcboot,tfdata] = ...
             newtimef(EEG.data(k,:,:), EEG.pnts,[EEG.xmin EEG.xmax]*1000, EEG.srate, 0, ...
              'padratio', 2, 'freqs', [0.5 40],'nfreqs', 41, ...
              'plotersp', 'off','plotitc','off','verbose','off');  
          
  Pow  = tfdata.*conj(tfdata); % power
          
  [~, AlpfBeg] = min(abs(freqs-alphafreq(1)));
  [~, AlpfEnd] = min(abs(freqs-alphafreq(2)));
  
  [~, ThetfBeg] = min(abs(freqs-thetafreq(1)));
  [~, ThetfEnd] = min(abs(freqs-thetafreq(2)));
  
  [~, SpinfBeg] = min(abs(freqs-spindlefreq(1)));
  [~, SpinfEnd] = min(abs(freqs-spindlefreq(2)));
                              
 % compute power in a frequency band..
   power_alphaFB = squeeze(sum(Pow(AlpfBeg:AlpfEnd,:,:),1));
   
   power_thetaFB = squeeze(sum(Pow(ThetfBeg:ThetfEnd,:,:),1));
   
   power_spindleFB = squeeze(sum(Pow(SpinfBeg:SpinfEnd,:,:),1));
   
   power_allFB = squeeze(sum(Pow(1:41,:,:),1));
   

 % mean across time points
   alpha(goodtrial,k) = mean(power_alphaFB);
   theta(goodtrial,k) = mean(power_thetaFB);
   
 
 %variance explained by different bands..
   
    varalpha(k,goodtrial) = 100 - 100*var(power_allFB - power_alphaFB)./var(power_allFB);
    vartheta(k,goodtrial) = 100 - 100*var(power_allFB - power_thetaFB)./var(power_allFB);
    varspindle(k,goodtrial) = 100 - 100*var(power_allFB - power_spindleFB)./var(power_allFB);
   
   
          
end

%Threshold the variances now..
varalpha(varalpha<20)=0; %30
vartheta(vartheta<20)=0;
varspindle(varspindle<20)=0;

%Find the favourable spatial points..
maxalpha(varalpha>max(vartheta,varspindle))=1;
maxtheta(vartheta>max(varalpha,varspindle))=1;
maxspindle(varspindle>max(varalpha,vartheta))=1;



talpha = sum(maxalpha)';
ttheta = sum(maxtheta)';
tspindle = sum(maxspindle)';

misc.talpha = talpha;
misc.ttheta = ttheta;
misc.tspindle = tspindle;


tmp_xalphatheta = corr(mean(theta,2),mean(alpha,2),'type','spearman','rows','complete');

% fprintf('\n\n -- Details for subject %s -- ', EEG.subject);
% if tmp_xalphatheta > 0
%             
%      fprintf('\n-- Possibly a noisy Subject --\n');
%      fprintf('-- Alpha and Theta correlate positively --\n');
%     
% end 



alphatrials(talpha>max(ttheta,tspindle)) = 1;


thetatrials(ttheta>max(talpha,tspindle)) = 1;


spindletrials(tspindle>max(talpha,ttheta)) = 1;


misc.alphatrials = alphatrials;
misc.thetatrials = thetatrials;
misc.spindletrials = spindletrials;
misc.alpha = alpha;
misc.theta = theta;

trials.alert = find(alphatrials>0)' ;
trials.drowsy = find(thetatrials>0)' ;
trials.deepdrowsy = find(spindletrials>0)' ;
trials.nondesc =  setdiff(1:ntrials,[trials.alert; trials.drowsy; trials.deepdrowsy]);


%if (isfield(EEG,'rejepoch'))
%    strtxt = sprintf('%.0f,',EEG.rejepoch');
%     fprintf('\nTrial numbers(%d) that were not considered are(rejected epochs):\n'...
%            , length(EEG.rejepoch));
%     fprintf('%s',strtxt(1:end-1));
%    tmp = setdiff(trials.nondesc, EEG.rejepoch);
%    strtxt = sprintf('%.0f,',tmp);
%end

%fprintf('\nTrial numbers(%d) that cannot be predicted are:\n',length(trials.nondesc));
%fprintf('%s',strtxt(1:end-1));

% fprintf('\nAlpha   predicts           :%3d trials', length(trials.alert));
% fprintf('\nTheta   predicts           :%3d trials', length(trials.drowsy));
% fprintf('\nSpindle predicts           :%3d trials', length(trials.deepdrowsy));
% fprintf('\nTrials not predicted       :%3d', length(trials.nondesc));
% fprintf('\n--------------------------------------------------------------');
% fprintf('\nTotal Trials accounted for :%3d', length(trials.alert) + length(trials.drowsy) ...
%          + length(trials.deepdrowsy) + length(trials.nondesc));
% fprintf('\n--------------------------------------------------------------\n');


end