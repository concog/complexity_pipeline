function results = calculate_mse(data, results)
% CALCULATE_MSE Calculate multiscale entropy

addpath '/home/ai04/Workspace/software/wfdb-app-toolbox-0-10-0/mcode'

% Number of scales
nscale = 10;

% Sampling frequency
fs = results.info.sampling_frequency;

% Get good epochs
timepoints = fieldnames(results.timepoints);

% Get channels / channel groups
channels = results.info.sensor_groups;

% Frequency bands
bands = results.info.specs.MSE.bands;

% Loop through timepoints
for timepoint = timepoints'
    timepoint = char(timepoint);
    % Add one to epochs because indices start from one in matlab
    epochs = results.timepoints.(timepoint).epochs+1;
    
    % Loop through frequency bands
    for band_idx = 1:size(bands,1)
        band = bands(band_idx, :);
        
        % Loop through channels / groups of channels
        channel_groups = fieldnames(channels);
        for channel_group = channel_groups'
            channel_group = char(channel_group);
            mse = zeros(numel(epochs),nscale);

            % Loop through good epochs in this timepoint
            for epoch_idx = 1:numel(epochs)
                % If using "group_channel", choose channels randomly such that
                % there are equal number of channels in each group (except
                % "glob")
                % Note: channels are randomised for each channel_group
                % although we use only channels for this channel_group.
                % This isn't probably even necessary since we're averaging
                % MSE over channels
                if results.info.python_script == "group_channel"
                    chosen_channels = choose_channels(channels);
                else
                    chosen_channels = channels;
                end

                % Get the data for this epoch and channel/group of channels
                data_epoch = data(chosen_channels.(channel_group), :, epochs(epoch_idx))';

                % Filter signals
                data_epoch = butter_filter(data_epoch, fs, 'bandpass', band, 5);

                % Average over channels in this group
                for chan_idx = 1:size(data_epoch,2)
                    % Calculate multi-scale entropy
                    cmse = msentropy(data_epoch(:, chan_idx), [], [], [], size(data_epoch,1), 1, 2, 2, nscale, 0.5, 0.5);
                    mse(epoch_idx, :) = mse(epoch_idx, :) + cmse';
                end
                mse(epoch_idx, :) = mse(epoch_idx, :) ./ size(data_epoch,2);

            end

            % Take the median over epochs
            mse = median(mse, 1);

            % Set the entropies into the results structure
            for scale_idx = 1:nscale
                results.locations.(channel_group).(to_matlab_key(band)).MSE.(['scale_' num2str(scale_idx)]).(timepoint) = mse(scale_idx);
            end 
        end
    end
end

end

function channels = choose_channels(channels)
% Use the same number of channels in all groups (except glob)
min_num_channels = min(cellfun('length',struct2cell(channels)));
groups = fieldnames(channels);
for group = groups'
    if group == "glob"; continue; end
    group = char(group);
    channels.(group) = unique(datasample(channels.(group), min_num_channels, 'replace', false));
end
end

% Struct field must start with letter, no commas
function matlab_key = to_matlab_key(band)

matlab_key = ['f' num2str(band(1)) '_' num2str(band(2)) 'Hz'];
matlab_key = strrep(matlab_key, '.', 'c');

end