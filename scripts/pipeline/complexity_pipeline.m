function complexity_pipeline(dataset, overwrite)
% COMPLEXITY_PIPELINE Pipeline for calculating LZ, PLE, power, alpha peaks
%etc.

% Define what will be calculated in the pipeline. 
% Note: 'group_channel' python script cannot currently handle re-processing
% of data (calculations in 'single_channel' are better structured).
% Note: the single_channel python script doesn't currently use 'calculate'
calculate = define_calculations(dataset);

% By default don't overwrite (mainly for debugging reasons, you should
% always overwrite previous files when actually running the pipeline)
if nargin < 2
    overwrite = false;
end

% Python needs to know where scripts / packages are
setenv('PYTHONPATH', '/home/ai04/Workspace/ai04_lib:/home/ai04/.local/lib')

% Also set python interpreter
python = '/imaging/local/software/anaconda/python3';

% Python script for calculating measures in python
folder = fileparts(mfilename('fullpath'));
python_file = fullfile(folder, sprintf('%s.py', dataset.python_script));

% If there are specific preparations that need to be done before running
% this pipeline do it now
dataset = dataset.prepare_for_pipeline();

% Get a list of subjects in the required dataset
dataset = initialise_subjects(dataset, overwrite);
filenames = dataset.get_processed_files();

% Open parallel processing pool
cbu_pool = gcp('nocreate');
if (~isempty(cbu_pool))
    fprintf('Using a pool with %d workers\n', cbu_pool.NumWorkers);
else
    fprintf('No parallel processing is used\n');
end

% Process subjects
parfor file_idx = 1:numel(filenames)
    current_file = filenames{file_idx};
    
    % Call the python script
    command = sprintf('%s %s %s', python, python_file, current_file);    
    
    % First run the python pipeline
    unix(command);
    
    % And then any matlab calculations we might have
    % Read the json file and decode it into a matlab structure
    json = fileread(current_file);
    results = jsondecode(json);
        
    % Open the data
    data = load(results.info.files.file_data);
    data = data.Data;
    
    % Calculate Chennu's graph-theoretic measures
    if isfield(calculate, 'chennu')
        results = calculate_graph_measures(results);
    end
    
    % Calculate alpha peaks
    if isfield(calculate, 'alpha_peaks')
        alpha_peaks = calculate_alpha_peaks(data, dataset.sampling_frequency, results.timepoints);
        results.alpha_peaks = alpha_peaks;
    end
        
    % Calculate spindles
    if isfield(calculate, 'spindles')
        spindles = calculate_spindles(results.info.files.file_eeglab, results.timepoints);
        results.spindles = spindles;
    end
    
    % Calculate multi-scale entropy
    if isfield(calculate, 'MSE')
        results = calculate_mse(data, results);
    end
    
    if isfield(calculate, 'functional_connectivity')
        results = calculate_functional_connectivity(data, results);
    end
    
    % ... Add here more pipeline calculations ...
 
    
    % Dataset-specific calculations here
    results = specific_calculations(dataset, data, results);
       
    % Save info about how long calculations took (could use this resume 
    % running a pipeline if it crashes / user stops it; just process those
    % files that don't have 'pipeline_finished' field)
    results.info.pipeline_finished = datestr(now, 'yyyy-mm-ddTHH:MM:SSZ');
    
    % Save the results again
    fid = fopen(results.info.files.file_processed, 'w');
    fprintf(fid, '%s', jsonencode(results));
    fclose(fid);
end

end


