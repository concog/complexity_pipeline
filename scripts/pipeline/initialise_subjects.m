function dataset = initialise_subjects(dataset, overwrite)
% INITIALISE_SUBJECTS Initialise files for each subject and return a list 
% of those subjects.
%
%   DATASET = INITIALISE_SUBJECTS(DATASET, OVERWRITE) makes sure that
%   complexity_pipeline can be run with this given dataset

% Check if there are already files in dataset.output_location and ask if
% user wants to destroy those. Also, create the folder if it doesn't exist
% NB: This is useful only for debugging, since the pipeline should be run
% with overwrite / from scratch when actually processing data
check_if_folder_exists(dataset.output_location, overwrite);

% Go through each subject
for subi = 1:numel(dataset.subjects)
    
    % File locations
    files = dataset.subjects(subi);
    
    % If processed file already exists, skip everything below
    % Note: this is only for debugging purposes, you should always run this
    % function with 'overwrite == true' when actually processing the data
    if exist(files.file_processed, 'file')
        continue;
    end

    % What we want for each subject
    % - location of data file
    % - location of the processed file
    % - location of artifact file
    % - valid epochs
    % - age, gender, iq, ...
  
    subject = struct();
    
    info = struct();
    info.dataset = dataset.name;
    info.python_script = dataset.python_script;
    info.sampling_frequency = dataset.sampling_frequency;
    info.files = files;

    % Save info about which calculations to do
    info.pipeline_started = datestr(now, 'yyyy-mm-ddTHH:MM:SSZ');

    % Save also more detailed info about specs and sensor groups
    if exist(dataset.file_specs, 'file') ~= 2
        dataset = dataset.issue_warning('Specs file is missing');
    else
        info.specs = jsondecode(fileread(dataset.file_specs));
    end

    if exist(dataset.file_sensor_groups, 'file') ~= 2
        dataset = dataset.issue_warning('Sensor groups file is missing');
    else
        info.sensor_groups = jsondecode(fileread(dataset.file_sensor_groups));
    end
    
    % Get and save timepoints
    subject.timepoints = dataset.get_timepoints(subi);
    
    % Save also number of good epochs per timepoint
    subject.num_good_epochs = get_number_of_good_epochs(subject.timepoints);

     % Save the info struct in case we need it later
    subject.info = info;
    
    % Save dataset-specific stuff
    subject = dataset.save_subject_info(subi, subject);
        
    % Save the data into files.processed_filename as JSON
    fid = fopen(files.file_processed, 'w');
    fprintf(fid, '%s', jsonencode(subject));
    fclose(fid);
    
end

end

% Get number of good epochs for each timepoint
function num_good_epochs = get_number_of_good_epochs(timepoints)
num_good_epochs = struct();
fields = fieldnames(timepoints);
for field = fields'
    field = char(field);
    num_good_epochs.(field) = numel(timepoints.(field).epochs);
end
end