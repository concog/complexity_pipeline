function results = calculate_functional_connectivity(data, results)
% CALCULATE_FUNCTIONAL_CONNECTIVITY Calculate (time-dependent) functional
% connectivity

window_size = results.info.specs.functional_connectivity.window_size;
overlap = results.info.specs.functional_connectivity.overlap;

if overlap >= window_size
    error('Overlap must be smaller than window size');
end

% Reshape data if it's not 2D
if ~ismatrix(data)
    siz = size(data);
    data = reshape(data, siz(1), siz(2)*siz(3));
end

nsamples = size(data, 2);

% Define windows
start_samples = 1 : window_size-overlap : nsamples-window_size+1;
end_samples = start_samples+window_size-1;

% Calculate in defined frequency bands
bands = results.info.specs.functional_connectivity.bands;

% Seems like sometimes json decoding outputs an [nbands x 2] array and
% sometimes an [2 x nbands] array; make sure 'bands' is in the former
% format
siz = size(bands);
if siz(2) ~= 2
    bands = bands';
end

for band_idx = 1:size(bands,1)
    band = bands(band_idx, :);

    % Filter data
    filtered_data = butter_filter(data', results.info.sampling_frequency, 'bandpass', band);
    
    % should we normalise? do we need to detrend if we're already filtering?

    % Loop through sliding windows
    timepoints = struct();
    for idx = 1:numel(end_samples)
        % Pearson or spearman correlation?
        timepoints.(sprintf('window_%d', idx)) = ...
            corr(filtered_data(start_samples(idx):end_samples(idx), :), 'type', 'pearson');
    end

    band_name = strrep(sprintf('f%s_%sHz', num2str(band(1)), num2str(band(2))), '.', 'c');    
    results.functional_connectivity.(band_name) = timepoints;
    
end
end

