import sys

import scipy.io
import numpy as np
import h5py
import json
import numbers
import warnings

from ai04_pylib.filters import bandpower
from ai04_pylib.filters import butter_filter
from ai04_pylib.util import normalise
from ai04_pylib.consciousness_measures import LZ
from ai04_pylib.consciousness_measures import PLE
from ai04_pylib.consciousness_measures.ZSFD import ZSFD
from ai04_pylib.consciousness_measures.SSE import SSE


def group_channel(processed_filename):

    # Load subject / calculation info
    with open(processed_filename) as data_file:
        results = json.load(data_file)

    # specs define which bandpass filtering is used, and which measures are calculated in which groups
    specs, group_indices = get_specs_and_sensor_groups(results)

    # Get min number of sensors
    number_of_channels_in_group = [len(group_indices[group]) for group in group_indices]
    min_number_channels = min(number_of_channels_in_group) if len(number_of_channels_in_group) > 0 else 0

    # Get sampling frequency
    fs = results["info"]["sampling_frequency"]

    # timepoints contain trial numbers for each timepoint (can be one or multiple epochs) that contain good quality data
    timepoints = results["timepoints"]

    # We need to know how many good epochs there are in each timepoint so that we can do some averaging later on. If
    # we're concatenating epochs then set num_good_epochs to one
    num_good_epochs = {}
    for timepoint in timepoints:
        if timepoints[timepoint]["type"] == "average":
            num_good_epochs[timepoint] = len(timepoints[timepoint]["epochs"])
        else:
            num_good_epochs[timepoint] = 1

    # Get frequency bands and corresponding (matlab) keys for power calculations
    power_bands, power_keys = get_frequency_bands(specs, ["power"], [], to_matlab_key)

    # Get all frequency bands that we should loop over (that are defined in results["calculate"] and specs)
    bands = get_frequency_bands(specs, [], ["power"])

    # Load data
    with h5py.File('%s' % results["info"]["files"]["file_data"], 'r') as f:
        d = f["Data"][:]

    # Add a singleton dimension of there's only one trial
    if d.ndim == 2:
        d = np.expand_dims(d, axis=0)

    # Go through each group and compute consciousness measures + power for each timepoint
    for group in group_indices:

        # Loop over timepoints
        for timepoint in timepoints:

            # Get good quality epochs; make them numpy vectors, i.e., shape is (n,) where n is number of elements
            good_epochs = np.asarray(timepoints[timepoint]["epochs"])

            # NOTE! Last minute changes here, you might need to do a np.squeeze(good_epochs) if good_epochs is not shaped (n,)

            # Loop over trials
            for trial_idx in range(num_good_epochs[timepoint]):
                # There are different numbers of channels in groups -- randomly sample using the number of smallest
                # group and subtract one because here indexing starts from zero
                all_channels = np.asarray(group_indices[group]) - 1

                # Get epoch(s)
                if timepoints[timepoint]["type"] == "average":
                    data_timepoint = d[good_epochs[trial_idx], :, all_channels].T
                else:
                    data_timepoint = d[good_epochs, :, all_channels[:, np.newaxis]]
                    # Concatenate epochs into one array
                    nchannels, nepochs, nsamples = np.shape(data_timepoint)
                    data_timepoint = np.transpose(data_timepoint, (2, 1, 0))
                    data_timepoint = np.reshape(data_timepoint, (nepochs*nsamples, nchannels), order='F')

                # Just a quick check if there are constant signals (could check for other stuff as well?)
                #constant_idxs = np.where(np.std(data_timepoint, axis=0) == 0)
                #if constant_idxs[0]:
                #    warnings.warn("Constant signals present!\n  File: {}\n  group: {}, timepoint: {}, trial_idx: {}"
                #                  .format(results["info"]["files"]["file_processed"], group, timepoint, trial_idx))

                # Estimate power in frequency bands, use all channels in group
                if 'power' in specs:
                    powers = bandpower(data_timepoint, fs, power_bands, power_keys)
                    #powers_log_norm = bandpower(data_timepoint, fs, power_bands, power_keys, log_normalised=True, nperseg_factor=2)
                    for matlab_key in powers:
                        add_to_results(results, ["locations", group, matlab_key, "power"], np.median(powers[matlab_key]), timepoint)
                    #    add_to_results(results, ["locations", group, matlab_key, "power_log_norm"], powers_log_norm[matlab_key], timepoint)

                # Loop through complexity measures
                for band in bands:
                    matlab_key = to_matlab_key(band)

                    # Filter data
                    filtered_timepoint = \
                        butter_filter("bandpass", data_timepoint, band, fs, order=5)

                    # Butterworth IIR filter can be unstable (shouldn't be with order=5), check here that the filtered
                    # signal's amplitudes are reasonable
                    if sum(sum(abs(filtered_timepoint))) / sum(sum(abs(data_timepoint))) > 5:
                        raise ValueError('Filter might be unstable')

                    # Normalise data
                    filtered_timepoint = normalise(filtered_timepoint)

                    # Choose the same number of channels for each group (where applicable)
                    selected_channels = np.random.choice(np.shape(filtered_timepoint)[1], min_number_channels, replace=False)
                    selected_channels = np.sort(selected_channels)

                    if calculate_this_measure("LZ", specs, group, band):
                        if "use_equal_num_sensors" not in specs["LZ"] or specs["LZ"]["use_equal_num_sensors"]:
                            # If we're using equal number of sensors for each group, then we should repeat calculations
                            # in glob group a couple of times
                            if group == "glob":
                                num_repetitions = 5
                            else:
                                num_repetitions = 1
                            LZ = calculate_LZ(filtered_timepoint[:, selected_channels], num_repetitions)
                        else:
                            LZ = calculate_LZ(filtered_timepoint, 1)

                        for measure in LZ:
                            add_to_results(results, ["locations", group, matlab_key, measure], LZ[measure], timepoint)

                    if calculate_this_measure("PLE", specs, group, band):
                        PLE = calculate_PLE(filtered_timepoint, fs)
                        add_to_results(results, ["locations", group, matlab_key, "PLE"], PLE, timepoint)

                    if calculate_this_measure("ZSFD", specs, group, band):
                        ZSFD = calculate_ZSFD(filtered_timepoint, fs)
                        add_to_results(results, ["locations", group, matlab_key, "ZSFD"], ZSFD, timepoint)

                    if calculate_this_measure("SSE", specs, group, band):
                        SSE = calculate_SSE(filtered_timepoint, band, fs)
                        add_to_results(results, ["locations", group, matlab_key, "SSE"], SSE, timepoint)

                    # IF YOU WANT TO CALCULATE STUFF IN EACH GROUP AND EACH FREQUENCY BAND, DEFINE THOSE CALCULATIONS HERE

        # We've saved the sums over trials, now we need to average them
        # TODO this averaging causes problems if we rerun pipeline without deleting old files
        if "locations" in results and group in results["locations"]:
            average_over_trials(results["locations"][group], num_good_epochs)

    # Add also alpha-theta ratios into the results so they'll be easily reachable
    if "AT_ratio" in specs:
        AT_ratio = calculate_AT_ratio(results["info"]["files"]["file_alphatheta"], timepoints)
        results["AT_ratio"] = AT_ratio

    # Save the results
    with open(processed_filename, 'w') as f:
        json.dump(results, f, ensure_ascii=False)


def formatted_warning(message, category, filename, fileno, lineno, file=None, line=None):
    return '%s:%s: %s\n' % (filename, category.__name__, message)


def calculate_SSE(filtered_timepoint, band, fs):
    # Input parameter 'band' defines cutoff frequencies (we don't want to
    # calculate entropy of filtered frequencies)
    trial_len = filtered_timepoint.shape[0]
    return SSE(filtered_timepoint, fs, trial_len, band)


def calculate_ZSFD(filtered_timepoint, fs):
    # Calculate zero-set fractal dimension for each second
    zsfd = np.zeros(filtered_timepoint.shape[1])

    # Go through all channels
    for idx in range(filtered_timepoint.shape[1]):
        data = filtered_timepoint[:, idx]
        fd = []
        begin_sample = 0
        end_sample = fs
        # Average over all 1 second segments in data
        while end_sample <= len(data):
            # We need to normalise here so that there will be zero crossings in the segment
            data_segment = normalise(data[begin_sample:end_sample])
            fd.append(ZSFD(data_segment, fs))
            begin_sample = end_sample
            end_sample = begin_sample + fs

        zsfd[idx] = np.mean(fd)

    return np.median(zsfd)


def calculate_LZ(filtered_timepoint, num_repetitions):
    LZc = 0
    LZc_col = 0
    LZsum = 0
    LZc_raw = 0
    for rep in range(num_repetitions):
        measures = LZ.LZc_LZsum(filtered_timepoint)
        LZc += measures["LZc"]
        LZsum += measures["LZsum"]
        LZc_raw += measures["LZc_raw"]
        LZc_col += measures["LZc_col"]

    return {"LZc": LZc/num_repetitions,
            "LZc_col": LZc_col/num_repetitions,
            "LZsum": LZsum/num_repetitions,
            "LZc_raw": LZc_raw/num_repetitions}


# We need a depth-first recursive search here => traverse dict until a scalar value is encountered
def average_over_trials(group_results, num_good_epochs):
    # Take the mean of each measure in each group and band
    for nested_field in group_results:
        if isinstance(group_results[nested_field], float):
            group_results[nested_field] /= num_good_epochs[nested_field]
        elif isinstance(group_results[nested_field], dict):
            average_over_trials(group_results[nested_field], num_good_epochs)
        else:
            raise TypeError("Couldn't recognize type " + type(group_results[nested_field]))


# Recursive function, traverse the path into a nested field in results and set the value/values
def add_to_results(results, path, value, timepoint):
    if len(path) == 0:
        # value can be scalar (LZ, power), or a dict (PLE)
        if isinstance(value, dict):
            # Add all values in the dict into results
            for value_key in value:
                if value_key not in results:
                    results[value_key] = {}
                add_to_results(results[value_key], [], value[value_key], timepoint)

        elif isinstance(value, float):
            # if path[0] in results:
            if timepoint in results:
                results[timepoint] += value
            else:
                results[timepoint] = value

        else:
            raise TypeError("Couldn't add " + value + " to results, unrecognized type (" + type(value) + "; must be dict or float)")

    else:
        # Traverse path deeper into results
        if path[0] not in results:
            results[path[0]] = {}
        add_to_results(results[path[0]], path[1:], value, timepoint)


def get_specs_and_sensor_groups(results):

    # Read specs and sensor groups
    specs = results["info"]["specs"]
    sensor_groups = results["info"]["sensor_groups"]

    # Collect sensor group indices in 'indices'
    indices = dict()

    # Loop through measures and save indices for groups that are used
    for measure in specs:
        if isinstance(specs[measure], dict) and "groups" in specs[measure]:
            for group in specs[measure]["groups"]:

                # Add this group into 'indices' if not added already
                if group not in indices:
                    indices[group] = sensor_groups[group]

    return specs, indices


def get_frequency_bands(specs, measures, ignore, key_producer=None):
    freq_bands = []

    # Go through measures in specs to get the frequency bands
    # If 'measures' is not defined, go through all keys in specs
    if len(measures) == 0:
        measures = list(specs.keys())

    # If 'ignore' is defined, ignore those measures
    if len(ignore) > 0:
        measures = list(set(measures) - set(ignore))

    for measure in measures:
        # Check if there are specs for this measure
        if measure in specs:
            # If there are no bands defined, skip this measure
            if not isinstance(specs[measure], dict) or "bands" not in specs[measure]:
                continue

            # Make sure specs[measure]["bands"] is a list of lists (stupid json won't parse a list of lists if there's
            # only one list in the list of lists)
            if isinstance(specs[measure]["bands"][0], numbers.Number):
                specs[measure]["bands"] = [specs[measure]["bands"]]

            # Loop through specified bands for this measure and add them to freq_bands
            for band in specs[measure]["bands"]:
                if band not in freq_bands:
                    freq_bands.append(band)

    # Convert to numpy arrays
    freq_bands = np.asarray(freq_bands)

    # Return the bands, and corresponding keys if key_producer is given
    if key_producer is None:
        return freq_bands
    else:
        keys = []
        for band in freq_bands:
            keys.append(key_producer(band))
        return freq_bands, keys


def calculate_AT_ratio(AT_filename, timepoints):
    # Load the matlab file containing alpha-theta ratios
    AT_ratio = scipy.io.loadmat(AT_filename)["O"][0][0][2]

    # Calculate the average ratio for each timepoint
    results = {}
    for timepoint in timepoints:
        results[timepoint] = np.mean(AT_ratio[timepoints[timepoint]["epochs"]])

    return results


def calculate_PLE(filtered_timepoint, fs):
    # Use the channels that are defined in PLE
    channels = PLE.MEG_channels

    # Calculate lag for this sampling rate. In the original paper they used lag
    # of 23 milliseconds
    lag = np.ceil(0.023/(1/fs))

    results = {}
    averages = {}

    for channel_type in channels:
        selected_channels = channels[channel_type]

        ple = PLE.PLE(filtered_timepoint[:, selected_channels], lag=lag, channels=selected_channels)
        average = 0
        for channel_pair in ple:
            average += ple[channel_pair]

        results[channel_type] = ple
        averages[channel_type] = average/len(ple.keys())

    # Calculate also averages for odd+even, distributed_odd+distributed_even
    averages["odd_even"] = (averages["odd"] + averages["even"])/2
    averages["distributed_odd_even"] = (averages["distributed_odd"] + averages["distributed_even"])/2

    results["averages"] = averages

    return results


# There can't be periods in the key and it must start with a character
def to_matlab_key(key):
    if isinstance(key, (np.ndarray, np.float64)):
        matlab_key = "f"
        first_value = True
        for value in key:
            if isinstance(value, (int, np.integer)) or value.is_integer():
                matlab_key += str(int(value))
            else:
                matlab_key += str(value).replace(".", "c")
            if first_value:
                matlab_key += "_"
                first_value = False
        matlab_key += "Hz"
        return matlab_key
    else:
        return "f" + key.replace(".", "c") + "Hz"


# Check if we should calculate this measure for this group & frequency band
def calculate_this_measure(measure, specs, group, band):
    if measure in specs:
        if group in specs[measure]["groups"] and list(band) in specs[measure]["bands"]:
            return True
    return False


# Format warnings
warnings.formatwarning = formatted_warning

# Run the script
processed_filename = sys.argv[1]
group_channel(processed_filename)
