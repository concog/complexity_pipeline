function vars = calcftspec_adapted(d, vars)

% Load list of frequencies
load(fullfile(vars.mohawk_path, 'code', 'Users', 'chennu', 'Work', 'MOHAWK', 'freqlist.mat'), 'freqlist');

% Calculate power spectra
cfg = [];
cfg.output     = 'pow';
cfg.method     = 'mtmfft';
cfg.foilim        = [0.01 40];
cfg.taper = 'dpss';
cfg.tapsmofrq = 0.3;

d = ft_freqanalysis(cfg, d);
spectra = d.powspctrm;
freqs = d.freq;


bpower = zeros(size(freqlist,1), numel(d.label));
for f = 1:size(freqlist,1)
    [~, bstart] = min(abs(freqs-freqlist(f,1)));
    [~, bstop] = min(abs(freqs-freqlist(f,2)));
    [~,peakindex] = max(mean(spectra(:,bstart:bstop),1),[],2);
    bpower(f,:) = spectra(:,bstart+peakindex-1);
end
for c = 1:size(bpower,2)
    bpower(:,c) = bpower(:,c)./sum(bpower(:,c));
end

% Save some output variables
%vars.freqs = freqs;
%vars.spectra = spectra;
%vars.freqlist = freqlist;
%vars.bpower = bpower;
end
    
