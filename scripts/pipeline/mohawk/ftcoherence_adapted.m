function matrix = ftcoherence_adapted(d, freqlist)

labels = d.label;

% Calculate cross spectra
cfg = [];
cfg.output     = 'powandcsd';
cfg.method     = 'mtmfft';
cfg.foilim        = [0.5 45];
cfg.taper = 'dpss';
cfg.tapsmofrq = 0.3;
cfg.keeptrials = 'yes';
cfg.pad='nextpow2';
%numrand = 0;

d = ft_freqanalysis(cfg, d);
%s = whos;
%fprintf('Using %f gigabytes\n', sum([s.bytes])/1073741824);

% Calculate weighted phase lag index / coherence
matrix = zeros(size(freqlist,1), numel(labels), numel(labels));
coh = zeros(numel(labels), numel(labels));

wpli = ft_connectivity_wpli(d.crsspctrm,'debias',true,'dojack',false);

for f = 1:size(freqlist,1)
    [~, bstart] = min(abs(d.freq-freqlist(f,1)));
    [~, bend] = min(abs(d.freq-freqlist(f,2)));
    [~,freqidx] = max(mean(wpli(:,bstart:bend),1));
    
    coh(:) = 0;
    coh(logical(tril(ones(size(coh)),-1))) = wpli(:,bstart+freqidx-1);
    coh = tril(coh,1)+tril(coh,1)';
    
    matrix(f,:,:) = coh;
end

end

