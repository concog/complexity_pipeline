function [d, chandist, freqlist, meeg] = prepare_data(mohawk_path, spm_file)

% Load spm
ai04_loadspm()

% Make sure fieldtrip is also in path
addpath(genpath(fullfile(mohawk_path, 'code', 'Users', 'chennu', 'MATLAB', 'fieldtrip')));

% Load the list of frequency bands
load(fullfile(mohawk_path, 'code', 'Users', 'chennu', 'Work', 'MOHAWK', 'freqlist.mat'), 'freqlist');

% Load the spm file and convert it to fieldtrip format
D = spm_eeg_load(spm_file);

% Figure out whether we're using EEG or MEG data
selected_labels = chanlabels(D);
meeg = selected_labels{1}(1:3);

% If were using MEG data, we need to ignore magnetometer channels (which
% are included in D.sensors('MEG'))
sens = D.sensors(meeg);
all_labels = string(sens.label);
selected_labels = string(selected_labels)';

% Find indices of sensors we want to retain
retain_idx = nan(size(selected_labels));
for label_idx = 1:numel(retain_idx)
    retain_idx(label_idx) = find(all_labels == selected_labels(label_idx));
end

% Remove extra sensors
chanpos = sens.chanpos(retain_idx, :);

% Then we need to calculate the distances between those sensors
chandist = squareform(pdist(chanpos));

% Transform into fieldtrip format
d = ftraw(D);

% Remove 'trialinfo' field from 'd', it's just causing problems later on
d = removefields(d, 'trialinfo');

end
