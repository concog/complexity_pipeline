function results = calculate_graph_measures(results)

% Use functionality in Chennu's MOHAWK app
% Why did matlab install MOHAWK in Documents? Maybe move it somewhere else?
mohawk_path = '/home/ai04/Documents/MATLAB/Add-Ons/Apps/MOHAWK';
addpath(genpath(mohawk_path));

% Let's keep the function names same as in MOHAWK so it's easier to debug
% if needed

% We need to load the data, list of frequency bands and calculate channel distances
[d, chandist, freqlist, meeg] = prepare_data(mohawk_path, results.info.files.file_spm);

% Calculate Chennu's metrics for each timepoint. 
% NOTE! If there are a set of timepoints defined in
% results.info.specs.chennu, these override the timepoints defined in
% results.timepoints
% ALSO NOTE! Timepoints' 'type' will be ignored in graph calculations
if isfield(results.info.specs.chennu, 'timepoints')
    timepoints = results.info.specs.chennu.timepoints;
else
    timepoints = results.timepoints;
end

for timepoint = string(fieldnames(timepoints))'
    timepoint = char(timepoint);
    epochs = timepoints.(timepoint).epochs;
    if numel(epochs) == 1
        error('Are you sure you want to calculate graph metrics for single epochs? Also, WPLI cannot be calculated for single epochs(?)');
    end
    matrix = ftcoherence_adapted(ft_selectdata(struct('trials', epochs+1), d), freqlist);
    results = calc_graph_metrics_with_different_sensor_configs(results, matrix, freqlist, timepoint, chandist, meeg);
end

% Remove MOHAWK from path
rmpath(genpath(mohawk_path));

end


function results = set_results(results, timepoint, config_type, graphdata, freqlist, avg)

% Get number of metrics
nmeasure = size(graphdata, 1);

% Save each metric into results
for measure_idx = 1:nmeasure
    
    % Get name of this measure
    measure_name = string(strrep(graphdata{measure_idx, 1}, ' ', '_'));
    measure = graphdata{measure_idx, 2};
    
    % Make sure something's calculated for this measure
    if isempty(measure)
        continue;
    end
    
    % Go through frequency bands
    for freq_idx = 1:size(freqlist,1)
        freq = "f" + num2str(freqlist(freq_idx, 1)) + "_" + num2str(freqlist(freq_idx, 2)) + "Hz";
    
        % Go through tvals
        if avg
            cmeasure = mean(mean(measure(freq_idx,:,:)));
        else
            cmeasure = squeeze(measure(freq_idx, :, :));
        end

        timepoint = char(timepoint);
        %if numel(timepoint) > 6 && timepoint(1:6) == "minute"
        %    query = ["chennu", "minute_wise", config_type, freq, measure_name, string(timepoint)];
        %else
        query = ["chennu", config_type, freq, measure_name, string(timepoint)];
        %end
                
        % Set this metric to 'results'
        results = setfield(results, query{:}, cmeasure);
    end
end
end


function graphdata = add_wpli(graphdata, matrix)

nchannels = size(matrix, 2);

wpli_values = zeros(size(matrix,1), (nchannels-1)*nchannels/2);
for freq_band = 1:size(matrix, 1)
    wpli_band = squeeze(matrix(freq_band, :, :));
    wpli_values(freq_band, :) = wpli_band(logical(tril(ones(nchannels), -1)));
end

graphdata(end+1, 1:2) = {'wpli', wpli_values};

end

function results = calc_graph_metrics_with_different_sensor_configs(results, matrix, freqlist, timepoint, chandist, meeg)

% If using MEG data calculate all different combinations (for now). If
% using EEG data calculate only "combined" i.e. do nothing.
%if meeg == "MEG"
%    configs = ["odd", "even", "averaged", "combined"];
%else
%    configs = "combined";
%end
configs = string(results.info.specs.chennu.channel_configs);

% Loop through all frequency bands
for freq_idx = 1:size(matrix,1)
    
    for config = configs'
        switch config
            case "odd"
                cmatrix = matrix(freq_idx, 1:2:end, 1:2:end);
                cchandist = chandist(1:2:end, 1:2:end);
            case "even"
                cmatrix = matrix(freq_idx, 2:2:end, 2:2:end);
                cchandist = chandist(1:2:end, 1:2:end);
            case "averaged"
                cmatrix = (matrix(freq_idx, 1:2:end, 1:2:end) + matrix(freq_idx, 2:2:end, 2:2:end)) ./ 2;
                cchandist = chandist(1:2:end, 1:2:end);
            case "combined"
                cmatrix = matrix(freq_idx, :, :);
                cchandist = chandist;
            otherwise
                error('Config %s not recognized', config);
        end
        
        graphdata = calcgraph_adapted(cmatrix, cchandist);

        % Add wpli/coherence matrix to graphdata
        graphdata = add_wpli(graphdata, cmatrix);

        % Set graphdata to 'results'
        results = set_results(results, timepoint, config, graphdata, freqlist(freq_idx,:), true);
        results = set_results(results, timepoint, config, {'wpli_array', cmatrix}, freqlist(freq_idx,:), false);
    end
end
end