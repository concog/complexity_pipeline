function graphdata  = calcgraph_adapted(matrix, chandist)

% Repeat 50 times
param = struct('heuristic', 50);

tvals = 1:-0.025:0.1;

% Normalise channel distances
chandist = chandist / max(chandist(:));
nchannels = size(chandist,1);

% Initialise graphdata
graphdata{1,1} = 'clustering';
graphdata{1,2} = zeros(size(matrix,1), numel(tvals), nchannels);
graphdata{2,1} = 'characteristic path length';
graphdata{2,2} = zeros(size(matrix,1), numel(tvals));
graphdata{3,1} = 'global efficiency';
graphdata{3,2} = zeros(size(graphdata{2,2}));
graphdata{4,1} = 'modularity';
graphdata{4,2} = zeros(size(graphdata{2,2}));
graphdata{5,1} = 'modules';
graphdata{5,2} = zeros(size(graphdata{1,2}));
graphdata{6,1} = 'centrality';
graphdata{6,2} = zeros(size(graphdata{1,2}));
graphdata{7,1} = 'modular span';
graphdata{7,2} = zeros(size(graphdata{2,2}));
graphdata{8,1} = 'participation coefficient';
graphdata{8,2} = zeros(size(graphdata{1,2}));
graphdata{9,1} = 'degree';
graphdata{9,2} = zeros(size(graphdata{1,2}));
%graphdata{10,1} = 'mutual information';


for f = 1:size(matrix,1)
    cohmat = squeeze(matrix(f,:,:));
    cohmat(isnan(cohmat)) = 0;
    cohmat = abs(cohmat);
    
    for thresh = 1:numel(tvals)
        bincoh = double(threshold_proportional(cohmat,tvals(thresh)) ~= 0);
                
        allQ = zeros(1, param.heuristic);
        allCi = zeros(param.heuristic, size(bincoh,1));
        allms = zeros(size(allQ));
        allpc = zeros(size(allCi));
        
        for i = 1:param.heuristic
            [Ci, allQ(i)] = community_louvain(bincoh);
            
            allCi(i,:) = Ci;
            
            modspan = zeros(1,max(Ci));
            for m = 1:max(Ci)
                if sum(Ci == m) > 1
                    distmat = chandist(Ci == m,Ci == m) .* bincoh(Ci == m,Ci == m);
                    distmat = nonzeros(triu(distmat,1));
                    modspan(m) = sum(distmat)/sum(Ci == m);
                end
            end
            allms(i) = max(nonzeros(modspan));
            allpc(i,:) = participation_coef(bincoh,Ci);
        end
    
        %clustering coeffcient
        graphdata{1,2}(f,thresh,1:nchannels) = clustering_coef_bu(bincoh);
        
        %characteristic path length
        graphdata{2,2}(f,thresh) = charpath(distance_bin(bincoh),0,0);
        
        %global efficiency
        graphdata{3,2}(f,thresh) = efficiency_bin(bincoh);
        
        % modularity
        graphdata{4,2}(f,thresh) = mean(allQ);
        
        % community structure
        graphdata{5,2}(f,thresh,1:nchannels) = allCi(1,:);
        
        %betweenness centrality
        graphdata{6,2}(f,thresh,1:nchannels) = betweenness_bin(bincoh);
        
        %modular span
        graphdata{7,2}(f,thresh) = mean(allms);
        
        %participation coefficient
        graphdata{8,2}(f,thresh,1:nchannels) = mean(allpc);
        
        %degree
        graphdata{9,2}(f,thresh,1:nchannels) = degrees_und(bincoh);
    end
end
end