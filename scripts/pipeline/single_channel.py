from pylab import *
import numpy as np
import h5py
import json

from ai04_pylib.filters import bandpower
from ai04_pylib.filters import butter_filter
from ai04_pylib.util import normalise
from ai04_pylib.consciousness_measures.LZ import LZc_LZsum
from ai04_pylib.consciousness_measures.ZSFD import ZSFD
from ai04_pylib.consciousness_measures.SSE import SSE


def single_channel(processed_filename):

    # Load the data
    with open(processed_filename) as data_file:
        results = json.load(data_file)

    # timepoints contain epoch numbers that contain good quality data
    timepoints = results["timepoints"]

    # specs define which bandpass filtering is used, and which measures are calculated
    specs = results["info"]["specs"]

    # Get sampling frequency
    fs = results["info"]["sampling_frequency"]

    with h5py.File('%s' % results["info"]["files"]["file_data"], 'r') as f:
        d = f["Data"][:]

    nchannels = d.shape[2]

    # Go through each group and compute consciousness measures + power for each timepoint
    for channel in range(nchannels):
        channel_name = "channel_" + str(channel+1)

        for timepoint in timepoints:
            timepoint_data = d[timepoints[timepoint], :, channel].T

            # Calculate LZc and powers and other stuff and add to results
            # There will be a lot of filtering and normalising (separately in
            # each calculate_ function), but it's better to have all functions
            # separated, makes the structure much cleaner. Anyway filtering
            # and normalising aren't the compute-intensive parts

            if "power" in specs:
                powers = calculate_powers(timepoint_data, specs, fs)
                add_to_results(results, ["locations", channel_name], powers, timepoint)

            if "SSE" in specs:
                sse = calculate_shannon_spectral_entropy(timepoint_data, specs, fs)
                add_to_results(results, ["locations", channel_name], sse, timepoint)

            if "LZ" in specs:
                lz = calculate_lempel_ziv(timepoint_data, specs, fs)
                add_to_results(results, ["locations", channel_name], lz, timepoint)

            if "ZSFD" in specs:
                zsfd = calculate_zero_set_fractal_dimension(timepoint_data, specs, fs)
                add_to_results(results, ["locations", channel_name], zsfd, timepoint)

    # Save the results
    with open(processed_filename, 'w') as f:
        json.dump(results, f, ensure_ascii=False)


def calculate_shannon_spectral_entropy(data, specs, fs):
    bands = np.asarray(specs["SSE"]["bands"])

    sse = dict()
    nsamples = data.shape[0]

    # Loop over all frequency bands
    for band in bands:
        filtered = butter_filter("bandpass", data, band, fs, order=5)
        sse[to_matlab_key(band)] = {"SSE": SSE(filtered, fs, nsamples, band)}

    return sse


def calculate_powers(data, specs, fs):
    # Estimate power in frequency bands

    bands = np.asarray(specs["power"]["bands"])
    keys = []
    for band in bands:
        keys.append(to_matlab_key(np.asarray(band)))

    powers = bandpower(data, fs, bands, keys)

    for band in powers:
        powers[band] = {"power": np.median(powers[band])}

    return powers


def calculate_zero_set_fractal_dimension(data, specs, fs):
    nchannels = data.shape[1]
    bands = np.asarray(specs["ZSFD"]["bands"])

    measures = dict()

    # Loop over trials
    for band in bands:
        matlab_key = to_matlab_key(band)
        fd = np.zeros(nchannels)

        for channel_idx in range(nchannels):
            # Get epoch
            data_channel = data[:, channel_idx]

            # Filter data
            filtered = butter_filter("bandpass", data_channel, band, fs, order=5)

            # Data is X seconds, calculate for each second
            fds = []

            begin_sample = 0
            end_sample = fs
            while end_sample <= len(filtered):
                data_segment = normalise(filtered[begin_sample:end_sample])
                fds.append(ZSFD(data_segment, fs))
                begin_sample = end_sample
                end_sample = begin_sample + fs

            fd[channel_idx] = median(fds)

        measures[matlab_key] = dict()
        measures[matlab_key]["ZSFD"] = np.median(fd)

    return measures


def calculate_lempel_ziv(data, specs, fs):
    nchannels = data.shape[1]
    bands = np.asarray(specs["LZ"]["bands"])

    measures = dict()

    # Loop over trials
    for band in bands:
        matlab_key = to_matlab_key(band)
        lzc = np.zeros(nchannels)
        lzsum = np.zeros(nchannels)
        lzcraw = np.zeros(nchannels)

        for channel_idx in range(nchannels):
            # Get epoch
            data_channel = data[:, channel_idx]

            # Filter data
            filtered = butter_filter("bandpass", data_channel, band, fs, order=5)

            # Normalise data
            filtered = normalise(filtered)

            # Calculate LZ
            LZ = LZc_LZsum(filtered)
            lzc[channel_idx] = LZ["LZc"]
            lzsum[channel_idx] = LZ["LZsum"]
            lzcraw[channel_idx] = LZ["LZc_raw"]

        measures[matlab_key] = dict()
        measures[matlab_key]["LZc"] = np.median(lzc)
        measures[matlab_key]["LZsum"] = np.median(lzsum)
        measures[matlab_key]["LZc_raw"] = np.median(lzcraw)

    return measures


# Recursive function, traverse the path into a nested field in results and set the value/values
def add_to_results(results, path, value, timepoint):
    if len(path) == 0:
        # value can be scalar (LZ, power), or a dict (PLE)
        if isinstance(value, dict):
            # Add all values in the dict into results
            for value_key in value:
                if value_key not in results:
                    results[value_key] = {}
                add_to_results(results[value_key], [], value[value_key], timepoint)

        elif isinstance(value, float):
            if timepoint in results:
                results[timepoint] += value
            else:
                results[timepoint] = value

        else:
            raise TypeError("Couldn't add " + value + " to results, unrecognized type (" + type(value) + "; must be dict or float)")

    else:
        # Traverse path deeper into results
        if path[0] not in results:
            results[path[0]] = {}
        add_to_results(results[path[0]], path[1:], value, timepoint)


# There can't be periods in the key and it must start with a character
def to_matlab_key(key):
    if type(key) == np.ndarray:
        matlab_key = "f"
        first_value = True
        for value in key:
            if isinstance(value, (int, np.integer)) or value.is_integer():
                matlab_key += str(int(value))
            else:
                matlab_key += str(value).replace(".", "c")
            if first_value:
                matlab_key += "_"
                first_value = False
        matlab_key += "Hz"
        return matlab_key
    else:
        return "f" + key.replace(".", "c") + "Hz"


# Run the script
def main():
    processed_filename = sys.argv[1]
    single_channel(processed_filename)


if __name__ == "__main__":
    main()
