classdef DataSetInterface
    % DataSetInterface Interface to handle datasets in the complexity
    % pipeline
    
    properties
        name                                  % Name of dataset
        dataset_location                      % Location of (preprocessed) data files
        output_location                       % Location where processed files will be saved into
        sampling_frequency                    % Sampling frequency
        python_script = 'group_channel'       % Name of python script that will be run (default 'group_channel')
        file_specs                            % File that contains calculation specs (a json file)
        file_sensor_groups                    % File that contains groupings of sensors (a json file)
        nsubjects = 0                         % Number of subjects
        subjects                              % Contains files and other info for all subjects 
        warnings = strings(0)                 % Contains warnings issued during pipeline / analysis
    end
    
    methods (Abstract)
        timepoints = get_timepoints(obj, subject_idx) % Returns timepoints
        %obj = load_files(obj) % Load files into respective properties variables
    end
    
    methods
        
        function subject_info = save_subject_info(obj, subject_idx, subject_info)
            % Save dataset-specific info about subjects before processing
            
            % Do nothing by default; override this method in subclasses
        end
        
        function results = specific_calculations(obj, data, results)
            % Dataset-specific calculations are defined here
            
            % Do nothing by default; override this method in subclasses
        end
        
        function obj = prepare_for_pipeline(obj)
            % Define here preparations that need to be done before using
            % this object in a complexity_pipeline, but are not needed when
            % using this object in a ComplexityDataSet.
            
            % This is usually empty; use only if you want to separate slow 
            % operations like reading age/IQ/etc from files that are not 
            % needed when using a ComplexityDataSet
        end
        
        function specs = define_calculations(obj)
            % Read the json file with specifications
            json = fileread(obj.file_specs);
            specs = jsondecode(json);
            
            for calculation = fieldnames(specs)'
                calculation = char(calculation);
                
                if isstruct(specs.(calculation))
                    specs.(calculation) = true;
                elseif ~islogical(specs.(calculation))
                    specs.(calculation) = false;
                end
            end
            
        end
                
        function obj = add_subject(obj, varargin)
            % Add subject to dataset
            
            % Get fieldnames
            fields = varargin(1:2:end);
            
            % Required files: file_data and file_artifact
            file_data_idx = find("file_data" == fields);
            if isempty(file_data_idx)
                obj = obj.issue_warning('file_data was not given -- you can''t run complexity_pipeline without this file');
            end
            
            if ~any("file_processed" == fields)
                obj = obj.issue_warning('file_processed was not given -- you can''t run complexity_pipeline without this file');
            end
            
            % Check that there's a variable named 'Data'
            varnames = who('-file', varargin{file_data_idx+1});
            if ~ismember('Data', varnames)
                obj = obj.issue_warning('''Data'' variable does not exist -- you can''t run complexity_pipeline without this variable');
            end
            
            % Get values
            values = varargin(2:2:end);
            
            % There should be equal number of both
            if numel(fields) ~= numel(values)
                error('Incorrect number of inputs; there should be an even number of inputs');
            end
            
            % Add all given info into subjects struct array
            obj.nsubjects = obj.nsubjects + 1;
            for field_idx = 1:numel(fields)
                obj.subjects(obj.nsubjects).(fields{field_idx}) = values{field_idx};
            end
        end
        
        function files_processed = get_processed_files(obj)
            files_processed = string({obj.subjects.file_processed}');
        end
        
        function obj = issue_warning(obj, msg)
            msg = string(msg);
            
            % Issue a warning only if similar warning hasn't already been
            % given
            if all(msg ~= obj.warnings)
                warning(msg);
                obj.warnings(end+1) = msg;
            end
            
            % Save all warnings into a log file though
            % TODO
        end

    end
end