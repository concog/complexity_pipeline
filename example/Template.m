classdef Template < DataSetInterface
    % Template Contains a template for a dataset.
    %   See DataSetInterface class for details of properties and methods
    
    % Dataset-specific properties. Enter here any properties that you might
    % need
    properties
    end
    
    methods
        function obj = Template()
            % In the constructor you should set all properties that were
            % inherited from DataSetInterface. You also need to set all
            % required filenames as is done in method load_files() in this
            % Template class.
            
            % Name of the dataset
            obj.name = "Template";
            
            % Location of output files (will be created if it doesn't
            % exist)
            obj.output_location = '/home/ai04/Workspace/projects/template/output/';
            
            % Sampling frequency in Hz
            obj.sampling_frequency = 250;
            
            % Location of specs.json file that contains specifications of
            % which measures to calculate in which frequency bins and
            % sensor groups
            obj.file_specs = fullfile(fileparts(mfilename('fullpath')), 'specs.json');
            
            % Location of a file that defines which subsets of sensors
            % we're interested in; this example file divides 204 Elekta
            % MEG's gradiometer sensors into four subsets of groups (plus
            % one global group of all sensors)
            obj.file_sensor_groups = fullfile(fileparts(mfilename('fullpath')), 'elekta_meg_gradiometers.json');

            % Location of data files -- this template assumes that each
            % subject's data is in a separate folder in this location
            obj.dataset_location = '/home/ai04/Workspace/projects/template/data/';
            
            % Load filenames
            obj = obj.load_files();
        end
        
        function timepoints = get_timepoints(obj, subject_idx)
            % Define timepoints -- that is, whether epochs are grouped into
            % chunks of one minute etc. This template implementation
            % uses each epoch as a timepoint which means that there will be
            % a measure for each epoch (instead of averaging measures over
            % multiple epochs).
            
            % Return timepoints; each epoch is a timepoint
            file_data = obj.subjects(subject_idx).file_data;
            
            % Figure out how many trials there are
            data = matfile(file_data);
            [~, ~, ntrials] = size(data, 'Data');
            
            % If you have a file that contains a list of artifactual epochs
            % you could use it here to remove those epochs
            
            % Load artifact info
            %file_artifact = obj.subjects(subject_idx).file_artifact;
            %artifacts = load(file_artifact);
            
            % Remove artifactual epochs
            %good_epochs = setdiff(1:ntrials, artifacts.badepochs)';
            good_epochs = (1:ntrials)';
            
            % Define timepoints, each epoch is a timepoint; the field names
            % can be whatever you want. However, if you're later using
            % ComplexityDataSet for reading results, the field names must
            % be equal and contain a running index (the index
            % will be used to sort the timepoints chronologically)
            timepoints = struct();
            for epoch = good_epochs'
                % Start indexing from zero
                timepoints.(['epoch_' num2str(epoch)]) = {epoch-1};
            end

        end
        
        function obj = load_files(obj)
            % Define files for each subject
            
            % Get subjects (each in a separate folder)
            subjects = dir(obj.dataset_location);

            % Remove non-folders
            remove_idxs = ~cell2mat({subjects.isdir});
            remove_idxs(1:2) = 1;
            subjects(remove_idxs) = [];

            % Basename for the files
            basename = 'basename_%s';

            % Loop through subjects and extract filenames
            for subject_idx = 1:numel(subjects)                

                % Get current subject
                subject = subjects(subject_idx).name;
                filename_subject = sprintf(basename, subject);
                
                % The data file must exist, otherwise skip this subject.
                % This file must contain a variable called "Data" that is a 
                % nchannels x nsamples x ntrials array
                file_data = fullfile(obj.dataset_location, subject, ['forpy_' filename_subject '.mat']);
                if exist(file_data, 'file') ~= 2
                    continue;
                end

                % A bunch of other files, not necessarily required to run
                % the pipeline.
                
                % SPM file; needed for MOHAWK's graph theory calculations
                % (although the code is pretty straightforward to change to
                % use EEGLAB files; I'm using SPM files because they
                % contain coordinates of sensors which are necessary to
                % calculate distances between sensors)
                file_spm = fullfile(obj.dataset_location, subject, filename_subject);
                
                % Artifact file, contains a list of bad epochs that should
                % be ignored; this file is optional, typically you would 
                % use it in get_timepoints method above.
                %file_artifact = fullfile(obj.dataset_location, subject, ['ArtFCT_' filename_subject '.mat']);
                
                % Alphatheta file, contains alpha-theta ratios for each
                % epoch; not really necessary
                %file_alphatheta = fullfile(obj.dataset_location, subject, ['A_T_3_5_gradonly_' filename_subject '.mat']);
                
                % EEGLAB file, necessary only if you want to use Sri's old
                % spindle scripts I think?
                %file_eeglab = fullfile(obj.dataset_location, subject, ['eeglab_' filename_subject '.set']);

                % Output file; must be specified
                file_processed = fullfile(obj.output_location, ['processed_' filename_subject '.json']);
                
                % You can specify other dataset-specific files here

                % Save file paths
                obj = obj.add_subject(...
                    'file_data', file_data, ...
                    'file_spm', file_spm, ...
                    'file_processed', file_processed ...
                );

            end

        end
        
        function results = specific_calculations(obj, data, results)
            % Dataset-specific calculations. Define here calculations that 
            % you want to do only for this dataset. This method will be
            % called separately for each subject in the later stages of the
            % pipeline.
            
            % Do nothing by default
        end
        
        function subject_info = save_subject_info(obj, subject_idx, subject_info)
            % Save dataset-specific info about subjects before processing.
            % This method will be called separately for each subject in
            % "initialise_subjects" function before the actual processing
            % starts in the pipeline.
            
            % Do nothing by default
        end
        
        function obj = prepare_for_pipeline(obj)
            % Define here preparations that need to be done before using
            % this object in a complexity_pipeline, but are not needed when
            % using this object in a ComplexityDataSet.
            
            % This is usually empty; use only if you want to separate slow 
            % operations like reading age/IQ/etc from files that are not 
            % needed when using a ComplexityDataSet
        end
        
    end

end