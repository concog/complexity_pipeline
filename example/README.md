# Example of how to use the pipeline

It's relatively straightforward to use the pipeline. You only need to 

1. Create a Matlab class that implements the [DataSetInterface](https://gitlab.com/concog/complexity_pipeline/blob/master/scripts/pipeline/DataSetInterface.m) class.
2. Specify which measures are calculated in a `specs.json` file.
3. Then an object of such class is given to [complexity_pipeline](https://gitlab.com/concog/complexity_pipeline/blob/master/scripts/pipeline/complexity_pipeline.m) function to run the pipeline.
4. The pipeline saves metrics for each subject in a separate JSON file. 
    - You can either write your own scripts to read / analyse those results, or 
    - You can use [ComplexityDataSet](https://gitlab.com/concog/complexity_pipeline/blob/master/scripts/analysis/ComplexityDataSet/ComplexityDataSet.m) class to read / analyse the results.

See the bottom of this page for more details of data structure and data manipulation.


## 0. Prerequisites
- **IMPORTANT** Your data must be saved in a .mat file as a `nchannels x nsamples x ntrials` array, separately for each subject. The variable must be called 'Data'.
- [ai04_lib](https://gitlab.com/concog/ai04_lib) must be in your PYTHONPATH and in Matlab's path

## 1. Implementing a DataSetInterface class
- [DataSetInterface](https://gitlab.com/concog/complexity_pipeline/blob/master/scripts/pipeline/DataSetInterface.m) is an abstract Matlab class (interface) that defines which properties must be set and which methods must be implemented in order to use run the pipeline. This class defines where raw data and processed data are located, and defines timepoints (for instance, which epochs belong to first minute, which epochs belong to second minute, etc.). This class also defines dataset and subject specific data (task scores, ages, IQs, etc.) that will be calculated in the `complexity_pipeline` or loaded when using `ComplexityDataSet`. This class is the only thing you msut implement if you want to process a new dataset in `complexity_pipeline`.
- See the file [Template.m](https://gitlab.com/concog/complexity_pipeline/blob/master/example/Template.m) for an example implementation. Also, you can check files [MEG700.m](https://gitlab.com/concog/camcan/blob/master/scripts/pipeline/MEG700.m) and [MEG280.m](https://gitlab.com/concog/camcan/blob/master/scripts/pipeline/MEG280.m) for additional examples (these files are currently in a hidden project, sorry).
- **Note** An implementation of the `DataSetInterface` class is given to both `complexity_pipeline` and `ComplexityDataSet` as an input argument (since it contains raw and processed data locations and information about calculations).

## 2. Specify which measures are calculated
- There's an example [specs.json](https://gitlab.com/concog/complexity_pipeline/blob/master/example/specs.json) file that shows how you can define which frequency bands and which channel groups are used to calculate power/complexity measures. The class implemented above should use a file like this. **Note**: if you don't want to calculate a measure erase it completely from the file.
- Channel groups are defined in another JSON file called [sensor_groups.json](https://gitlab.com/concog/complexity_pipeline/blob/master/example/sensor_groups.json). Note that these groups are defaults that are loaded for each subject and then saved into the pipeline output file, which are then used in the actual calculations. Thus, you can modify the channel groups run-time after they've been loaded if you want to use different channels for each subject. **Note**: these channels represent parcels when you're using fMRI data.

## 3. Run the pipeline
- First start a parallel pool if you wish to use Matlab's parallel processing (recommended, some parts of the pipeline can be slow)
- Run the pipeline with: `complexity_pipeline(Template())`, where `Template()` is the class you implemented in step **1.**. If there's data saved in the output folder the program will ask if you want to remove that data before continuing. You can give the `complexity_pipeline` function a boolean true as a second input parameter to always remove existing data. **Note**: If you're rerunning the pipeline you should always remove any existing data.

## 4. Check the results
- After the pipeline has finished, there should be a JSON file containing the power/complexity measures for each subject in the `output_location` you defined in your class. You can e.g. load the results for one subject by typing 
```
json = fileread('/path/to/output/location/processed_subject1.json'); 
d = jsondecode(json);
```
Now struct `d` contains all measures that were calculated for the subject. 

### Use ComplexityDataSet
- If you want to use existing functionality to read / analyse outputs of the pipeline, you can use the [ComplexityDataSet](https://gitlab.com/concog/complexity_pipeline/blob/master/scripts/analysis/ComplexityDataSet/ComplexityDataSet.m) class. You can instantiate an object of this class by calling the constructor with the class you defined in step **1.** as first argument; the second argument should contain **queries** that define which measures are loaded from the files. 
- `ComplexityDataSet`'s functions often use parallel processing so it works faster with a parallel pool open.

### ComplexityDataSet example
- Define a set of queries; for example `LZc`, `LZsum` and `LZc_col` in `0.5-30Hz` band, and `MSE scale 1` in `left frontal` and `right frontal` sensors and in `0.5-30Hz` frequency band
```
queries = { ...
{'locations', 'occipital', 'f0c5_30Hz', ["LZc", "LZsum", "LZc_col"]}, ...
{'locations', ["left_frontal", "right_frontal"], 'f0c5_30Hz', 'MSE', 'scale_1'} ...
} 
```
- A set of queries is a cell array of cell arrays; typically you want to define one query (cell array) per feature, which is not what we do above. As above, it is possible to to use string vectors inside a query as a shorthand to define multiple queries (features). You can use `parse_queries` function to parse a set of queries into individual queries (i.e. feature names).
```
parsed_queries = parse_queries(queries)
::parsed_queries are shown below::
{
{'locations', 'occipital' ,'f0c5_30Hz', 'LZc'}
{'locations', 'occipital', 'f0c5_30Hz', 'LZsum'}
{'locations', 'occipital', 'f0c5_30Hz', 'LZc_col'}
{'locations', 'left_frontal', 'f0c5_30Hz', 'MSE', 'scale_1'}
{'locations', 'right_frontal', 'f0c5_30Hz', 'MSE', 'scale_1'}
}
```
- The char vectors / strings in queries are actually field names of the JSON structs of individual subjects. You can query any combination of (nested) field names you wish.
- Now load results for all subjects (use a parallel pool for faster processing)
```
dataset = ComplexityDataSet(Template(), queries);
```
- You can get the actual data with `ComplexityDataSet`s `get_data` method
```
features = dataset.get_data(queries)
``` 
returns the data defined in `queries`. Use `parse_queries` function to figure out which data corresponds to which feature in `features`: `parse_queries` returns features (or their names) in the same order as `get_data`.
- If you want to load more data into the `ComplexityDataSet` object, you must use `add_features` method (which takes queries as input)
```
dataset = dataset.add_features({{"gray_matter"}, {"white_matter"}, {"age"}})
```
- `ComplexityDataSet` contains a vector of `ComplexitySubject` objects, which contain the actual pipeline outputs (only those outputs that have been loaded when the `ComplexityDataSet` has been initialised, or that have been loaded with `add_features` method). 
- **Note** this functionality is still very much under development, especially for subjects that have different numbers of epochs (can't use double arrays, must use cell arrays), so always go through the code yourself to see what's actually happening.
- There are a few analysis / plot scripts in the [analysis folder](https://gitlab.com/concog/complexity_pipeline/tree/master/scripts/analysis) that can be used with a `ComplexityDataSet`. 


## Details of data structure and data manipulation

### Data structure

Again, to load the pipeline outputs for one subject, type `d = jsondecode(fileread('...'));`. This returns a Matlab struct. Innermost fields of a struct are called *timepoints*. Each timepoint has a *value*: this value can be a scalar, a vector, a matrix, or a N-dimensional matrix. If there are no timepoints (i.e. the innermost field of a query is a value), then the value is loaded as a **ConstantFeature**, meaning that the value does not change across timepoints (an example of such a query/feature could be *{{age}}* or *{{gender}}*). If there are timepoints, the value is loaded as a **VariableFeature**, meaning that the values may change across timepoints (an example of such a query/feature could be *{{'locations', 'glob', 'f0c5_30Hz', 'LZc'}}*. In user experience point-of-view this doesn't matter -- you don't need to know which feature is a **ConstantFeature** and which is a **VariableFeature**, all functions work with both.


### Data manipulation

#### ComplexityDataset.get_data
Highest level data getter, returns data for all subjects in a ComplexityDataSet. Works only when there are equal number of timepoints for each subject -- e.g. in *StopNoGo* class where we have 1 second pre-trial epochs the subjects have different numbers of trials, therefore you can't use *ComplexityDataSet.get_data* with that class, instead you need to load data separately for each subject.

Can return data for multiple features at a time. If all features have scalar values, the data is returned in an array with subjects in the first dimension, timepoints in the third dimension, and number of features in second dimension. If the values are not scalars, their dimensions are between second and last dimension (if I remember correctly, or perhaps timepoints are always third dimension and values' dimensions are the last ones; we've been using mostly scalars so far).

#### ComplexitySubject.get_data
Returns data for one subject. Use this if there are unequal number of timepoints for subjects (e.g. *StopNoGo* class mentioned above).

