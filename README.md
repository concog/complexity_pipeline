# Power / Complexity Pipeline

This pipeline calculates power and informational complexity related measures. Can be used with any kind of data (EEG, MEG, fMRI), as long as it's in the correct format (see the [example](https://gitlab.com/concog/complexity_pipeline/tree/master/example)).


## Power and informational complexity measures

### Power measures
- Power spectral density estimated with Welch's method in multiple frequency bands and groups of sensors
    - Frequency bands
        - 2 Hz bins between 0 Hz and 20 Hz (that is, 0-2 Hz, 2-4 Hz, ..., 18-20 Hz)
        - 10 Hz bins between 20 Hz and 100 Hz (that is, 20-30 Hz, 30-40 Hz, ..., 90-100 Hz)
        - Broadband bins: 0.5-30 Hz, 3-5 Hz, 8-12 Hz, 13-30 Hz
    - Sensor groups
        - Frontal
        - Left frontal
        - Right frontal
        - Occipital
        - Global (all sensors)


### Informatinal complexity measures
- The following complexity measures are calculated in multiple frequency bands and groups of sensors

Complexity measure | Abbreviation
--- | ---
Lempel Ziv sum | LZsum
Lempel Ziv complexity (both observation and channel concatenated versions) | LZc, LZc_col
Multi-scale entropy | MSE
Phase-lag entropy | PLE
Shannon spectral entropy | SSE
Zero-set fractal dimension | ZSFD

- All measures excluding PLE are calculated in the following frequency bands and groups of sensors
    - Frequency bands
        - Broadband bins: 0.5-30 Hz, 3-5 Hz, 8-12 Hz, 13-30 Hz
    - Sensor groups
        - Frontal
        - Left frontal
        - Right frontal
        - Occipital
        - Global (all sensors)

    - PLE is calculated also in 0.5-8 Hz and 8-30 Hz bands, and only using two different sets of sensors.

## Example

For an example of how to use the pipeline see the [example](https://gitlab.com/concog/complexity_pipeline/tree/master/example) folder.

